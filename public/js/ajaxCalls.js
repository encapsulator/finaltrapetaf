
$(function() {
    
    //Scoreboard change ordering table
    function handleOrderingScoreboard(){
        $.ajax({
            type: 'POST',
            cache: false,
            url: 'leaderboard',
            data: $('form#orderform').serialize(),
            dataType: 'json',
            success: function(data){
                    window.scrollTo(0, 0);
                $("#scoredata").slideUp("fast", function(){
                $("#scoredata").empty();
                 if(data.success == true){

                    var arr = data.orderdata;
                    var counter = 1;
                    $.each(arr, function(index, value){
                        if(counter != arr.to+1){
                        var collg = "<div class='col-xs-6 col-sm-4 col-md-3 col-lg-2'>";
                        var colsm = "<div class='col-xs-6 col-sm-4 col-md-3 col-lg-1'>";
                        var fetchdata = "<div class='row data'>"+colsm+counter+"</div>"+collg +value.username+"</div>"+collg+value.groepnaam+"</div>"
                        +colsm+value.amount+"</div>"+collg+value.avg_time+"</div>"+collg+value.stairs+"</div>"+collg+value.cal
                        +"</div></div>";
                        $("#scoredata").append(fetchdata);
                        }
                        counter++;
                    });
                    
                    $("#scoredata").append();
                    $("#scoredata").slideDown();
                    }
                });
            } 
        });
    }


    

    //Verberg div validation-errors na klik op kruisje
    $('body').on('click', 'a.close', function() {
	  $("#validation-errors").slideUp();
	});
     
   //Hide 'registreer met facebook' div aanmeldpagina na body load
   $("iframe#fbregister").hide();

   //Klik op registreren met facebook knop, slidedown div
   $('body').on('click', 'a#facebook', function() {
    	$("#formulier").hide();
    	$("iframe#fbregister").slideDown();
    });

    //scoreboard change order by
    $("#orderby").on('change', function(){
        handleOrderingScoreboard();
    });
    
    //scoreboard change order type asc/desc
    $("#ordertype").on('change', function(){
        handleOrderingScoreboard();
    });

   //AJAX call loginformulier
    $('#loginform').submit( function(){
		$.ajax({
                    type: 'POST',
                    cache: false,
                    url: 'login',
                    data: $('form#loginform').serialize(),
                    dataType: 'json',
                    beforeSend: function(){
                    	$("#validation-errors").html("<a class='close' href='#'>×</a>  ");                 
                	},
                	success: function(data) {
                            window.scrollTo(0, 0);
                		if(data.success == false){
                			var arr = data.errors;
                			$.each(arr, function(index, value){
                				if(value.length!=0){
                					$("#validation-errors").append("<p>"+value+"</p>");
                                }
                			});
                			$("#validation-errors").slideDown();
                		} else {
                			$("#formulier").load("login #formulier");
                            $("nav").load("login nav");
                		}
                	},
                	error: function(xhr, textStatus, thrownError){
                		$("#validation-errors").append('<p>Er ging iets mis, probeer later opnieuw.</p>');
                		$("#validation-errors").slideDown();
                	}

                });
                return false;		
        });
   //AJAX call aanmeldformulier	
	 $('#aanmeldform').submit( function(){
		$.ajax({
                    type: 'POST',
                    cache: false,
                    url: 'aanmelden',
                    data: $('form#aanmeldform').serialize(),
                    dataType: 'json',
                    beforeSend: function(){
                    	$("#validation-errors").empty();
                        $("#validation-errors").html("<a class='close' href='#'>×</a>  ");
                	},
                	success: function(data) {
                            window.scrollTo(0, 0);
                		if(data.success == false){
                			var arr = data.errors;
                			$.each(arr, function(index, value){
                				if(value.length!=0){
                					$("#validation-errors").append("<p>"+value+"</p>");
                                }
                			});
                			$("#validation-errors").slideDown();
                		} else {
                        	$("#validation-errors").toggleClass("alert-danger");
                        	$("#validation-errors").toggleClass("alert-success");
                        	$("#validation-errors").append("<p>De gegevens werden succesvol verwerkt!</p>");
                        	$("#validation-errors").slideDown();
                		}
                	},
                	error: function(xhr, textStatus, thrownError){
                		$("#validation-errors").append('<p>'+ thrownError + '</p>');
                		$("#validation-errors").slideDown();
                	}
                });
        return false;		
    });
});