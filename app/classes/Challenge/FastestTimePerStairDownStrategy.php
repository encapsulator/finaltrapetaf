<?php
namespace Challenge;
use Challenge\ChallengeStrategy;
use App\models;

class FastestTimePerStairDownStrategy extends ChallengeStrategy {
	
	public function __construct($groep){
		$this->groupfilter = $groep; 
		$this->ordering = 'ASC';
	}

	public function getBaseQuery(){
		return \DB::table('user')->join('traject', 'user.id', '=', 'traject.userid')
		->join('groep', 'groep.id', '=', 'user.groepid')
		->join('traject AS traject2', 'traject.id', '=', 'traject2.id')
		->where('traject2.treden_omhoog', '=', '0');
	}
	
	public function getSelectQuery($query){
		return $query->select( array( 
		\DB::raw( 'CONCAT(user.voornaam, " ", user.naam) AS username' ), 
		'groep.naam AS groepnaam', 
		\DB::raw( 'COUNT( traject.userid ) AS amount' ), 
		\DB::raw( 'SUM( traject.treden ) AS stairs' ), 
		\DB::raw( 'SUM( traject2.tijd ) / ( SUM( traject.treden ) - SUM( traject.treden_omhoog ) ) AS special' ),
		\DB::raw( 'ROUND( AVG( traject.tijd ) , 2 ) AS avg_time' )));
	}

}