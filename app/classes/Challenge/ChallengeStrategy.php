<?php

namespace Challenge;

abstract class ChallengeStrategy {
	
	protected $ordering;
	protected $groupfilter;
	
	public abstract function getBaseQuery();
	
	public abstract function getSelectQuery( $query );
	
	public function getRankingBetween( $dtf, $dtt ) {
		$query = $this->getBaseQuery();
		
		$query->where( 'traject.datum', '>=', $dtf->format( 'Y-m-d H:i:s' ))->where( 'traject.datum', '<', $dtt->format( 'Y-m-d H:i:s' ) );
		
		$query = $this->getSelectQuery( $query );
		
		$query = $this->getGroupAndOrdering( $query, $this->ordering );
		
		return $query->get();
	}
	
	public function getReverseRankingBetween( $dtf, $dtt ) {
		$query = $this->getBaseQuery();
		
		$query->where( 'traject.datum', '>=', $dtf->format( 'Y-m-d H:i:s' ) )->where( 'traject.datum', '<=', $dtt->format( 'Y-m-d H:i:s' ) );
		
		$query = $this->getSelectQuery( $query );
		
		$query = $this->getGroupAndOrdering( $query, $this->ordering == 'ASC' ? 'DESC' : 'ASC' );
		
		return $query->get();
	}
	
	public function getGroupAndOrdering( $query, $ordering ) {
		if ( $this->groupfilter != 0 ) {
			$query = $query->where( 'user.groepid', '=', $this->group );
		} 
		
		$query = $query->groupBy( 'user.id' )->having( \DB::raw( 'COUNT(traject.userid)'), '>', 0 )->orderBy( 'special', $ordering );
		
		return $query;
	}
	
}