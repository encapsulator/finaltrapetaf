<?php
namespace Challenge;
class ChallengeManager
{
	private $challenge;
	private $challengeStrategy;
	public function initStrategy( $challenge )
	{
		$this->challenge = $challenge;
		switch ( $challenge->ordering ) {
			case "mostStairsStrategy":
				$this->setStrategy( new MostStairsStrategy( $challenge->groupid ) );
				break;
			case "fastestTimePerStairUpStrategy":
				$this->setStrategy( new FastestTimePerStairUpStrategy( $challenge->groupid ) );
				break;
			case "fastestTimePerStairDownStrategy":
				$this->setStrategy( new FastestTimePerStairDownStrategy( $challenge->groupid ) );
				break;
			case "fastestTimePerStairStrategy":
				$this->setStrategy( new FastestTimePerStairStrategy( $challenge->groupid ) );
				break;
			case "fastestAvarageTimeStrategy":
				$this->setStrategy( new FastestAvarageTimeStrategy( $challenge->groupid ) );
				break;
			case "mostStairsUpPerStairStrategy":
				$this->setStrategy( new MostStairsUpPerStairStrategy( $challenge->groupid ) );
				break;
			case "mostTimesTakenStairsStrategy":
				$this->setStrategy( new MostTimesTakenStairsStrategy( $challenge->groupid ) );
				break;
			case "mostTimesDownStrategy":
				$this->setStrategy( new MostTimesDownStrategy( $challenge->groupid ) );
				break;
			case "mostTimesUpStrategy":
				$this->setStrategy( new MostTimesUpStrategy( $challenge->groupid ) );
				break;
			case "mostStairsDownStrategy":
				$this->setStrategy( new MostStairsDownStrategy( $challenge->groupid ) );
				break;
			case "mostStairsUpStrategy":
				$this->setStrategy( new MostStairsUpStrategy( $challenge->groupid ) );
				break;
			case "mostCaloriesBurnedStrategy":
				$this->setStrategy( new MostStairsStrategy( $challenge->groupid ) );
				break;
		}
	}
	public function setStrategy( $strategy )
	{
		$this->challengeStrategy = $strategy;
	}
	public function getRankingNthInterval( $n )
	{
		$dates = $this->getBeginDateEndDateNthInterval( $n );
		$now   = new \DateTime( "now" );
		if ( $dates[ 0 ]->format( "Y-m-d" ) > $this->getEndDate()->format( "Y-m-d" ) || $now->format( "Y-m-d" ) < $dates[ 0 ]->format( "Y-m-d" ) ) {
			throw new ChallengeIntervalNotFoundException( "Interval " . $n . ' not found' );
			;
		}
		if ( $this->challenge->reverse == 1 ) {
			return $this->getReverseRankingBetween( $dates[ 0 ], $dates[ 1 ] );
		}
		return $this->getRankingBetween( $dates[ 0 ], $dates[ 1 ] );
	}
	public function getBeginDateEndDateNthInterval( $n )
	{
		$totaldaysto   = $this->getTimeInterval()->format( "%d" ) * $n;
		$totaldaysfrom = $this->getTimeInterval()->format( "%d" ) * ( $n - 1 );
		$dateTo        = $this->getStartDate()->add( new \DateInterval( 'P' . $totaldaysto . 'D' ) );
		$dateFrom      = $this->getStartDate()->add( new \DateInterval( 'P' . $totaldaysfrom . 'D' ) );
		return array(
			 $dateFrom,
			$dateTo 
		);
	}
	public function getRankingCurrentInterval( )
	{
		$now = new \DateTime( "now" );
		if ( $this->challenge->reverse == 1 ) {
			return $this->getReverseRankingBetween( $this->getStartDateLastInterval(), $now );
		}
		return $this->getRankingBetween( $this->getStartDateLastInterval(), $now );
	}
	public function getRankingBetween( $datef, $datet )
	{
		return $this->challengeStrategy->getRankingBetween( $datef, $datet );
	}
	public function getReverseRankingBetween( $datef, $datet )
	{
		return $this->challengeStrategy->getReverseRankingBetween( $datef, $datet );
	}
	public function getAllTimeRanking( )
	{
		if ( $this->challenge->reverse == 1 ) {
			return $this->challengeStrategy->getReverseRankingBetween( $this->datestart, new DateTime( "now" ) );
		}
		return $this->challengeStrategy->getRankingBetween( $this->datestart, new Datetime( "now" ) );
	}
	public function listDoneIntervals( )
	{
		$allIntervals = array( );
		$now          = new \Datetime( "now" );
		$sum          = $this->getStartDate();
		if ( !isset( $this->challenge->timeInterval ) ) {
			return null;
		}
		if ( $now->format( "Y-m-d" ) >= $sum->format( "Y-m-d" ) ) {
			$i = 0;
			while ( $now->format( "Y-m-d" ) >= $sum->format( "Y-m-d" ) ) {
				$intervalStart   = new \Datetime( $sum->format( "Y-m-d" ) );
				$sum             = $sum->add( $this->getTimeInterval() );
				$intervalEnd     = new \Datetime( $sum->format( "Y-m-d" ) );
				$allIntervals[ ] = array(
					 $intervalStart,
					$intervalEnd 
				);
				$i++;
			}
		}
		return $allIntervals;
	}
	public function getRankingBoard( )
	{
		if ( isset( $this->challenge->timeInterval ) ) {
			return $this->getRankingCurrentInterval();
		}
		if ( !$this->challenge->reverse ) {
			return $this->challengeStrategy->getRankingBetween( $this->getStartDate(), $this->getEndDate() );
		}
		return $this->challengeStrategy->getReverseRankingBetween( $this->getStartDate(), $this->getEndDate() );
	}
	private function getTimeInterval( )
	{
		$days    = "P" . $this->challenge->timeInterval . "D";
		$dateint = new \DateInterval( $days );
		return $dateint;
	}
	public function setStartDateLastInterval( $date )
	{
		$this->challenge->startDateLastInterval = $date;
		$this->challenge->save();
	}
	private function getStartDate( )
	{
		return new \Datetime( $this->challenge->datestart );
	}
	private function getEndDate( )
	{
		return new \Datetime( $this->challenge->dateend );
	}
	private function getStartDateLastInterval( )
	{
		return new \Datetime( $this->challenge->startDateLastInterval );
	}
	public function checkForEndCurrentInterval( )
	{
		$now = new \Datetime( "now" );
		$sum = $this->getStartDateLastInterval()->add( $this->getTimeInterval() );
		if ( $now->format( "Y-m-d" ) >= $sum->format( "Y-m-d" ) ) {
			while ( $now->format( "Y-m-d" ) >= $sum->format( "Y-m-d" ) ) {
				$this->setStartDateLastInterval( $sum->format( "Y-m-d" ) );
				$sum = $sum->add( $this->getTimeInterval() );
			}
			$this->challenge->save();
			if ( null != $this->challenge->beschrijvingNewInterval ) {
				$this->makeNewIntervalPost();
			}
		}
	}
	public function checkForendChallenge( )
	{
		if ( $this->isFinished() && null != $this->challenge->beschrijvingEnd ) {
			$this->makeEndChallengePost();
		}
	}
	public function getTimeToLive( )
	{
		$now        = new Datetime( 'now' );
		$difference = $now->diff( $this->getStartDateLastInterval()->add( $this->getTimeInterval() ) );
		return $difference;
	}
	public function isFinished( )
	{
		$now = new \Datetime( 'now' );
		return ( $now->format( "Y-m-d" ) > $this->getEndDate()->format( "Y-m-d" ) );
	}
	private function makeNewIntervalPost( )
	{
		$data = array(
			 "title" => "De " . $this->challenge->naam . " challenge werd gereset, bekijk de resultaten!",
			'beschrijving' => $this->challenge->beschrijvingNewInterval,
			"thumbnail" => $this->challenge->thumbnail,
			"challenge" => str_replace( " ", "_", $this->challenge->naam ),
			"foto" => null,
			"article" => null 
		);
		$this->createNewNotification( $data );
	}
	private function makeEndChallengePost( )
	{
		$data = array(
			 "title" => "De " . $this->challenge->naam . " challenge werd beëindigd!",
			'beschrijving' => $this->challenge->beschrijvingNewInterval,
			"thumbnail" => $this->challenge->thumbnail,
			"challenge" => str_replace( " ", "_", $this->challenge->naam ),
			"foto" => null,
			"article" => null 
		);
		$this->createNewNotification( $data );
	}
	private function createNewNotification( $data )
	{
		$p = new \Post;
		$p->make( $data );
		$p->save();
	}
}