<?php
namespace Challenge;
use Challenge\ChallengeStrategy;
use App\models;

class FastestAvarageTimeStrategy extends ChallengeStrategy {
	
	public function __construct( $groep=0 ){
		$this->groupfilter = $groep; 
		$this->ordering = 'ASC';
	}

	public function getBaseQuery(){
		return DB::table('user')->join('traject', 'user.id', '=', 'traject.id')
				->join('groep', 'groep.id', '=', 'user.groepid');
	}

	public function getSelectQuery( $query ){
		return $query->select( array( 
			\DB::raw( 'CONCAT(user.voornaam, " ", user.naam) AS username' ), 
			'groep.naam AS groepnaam', 
			\DB::raw( 'COUNT( traject.userid ) AS amount' ), 
			\DB::raw( 'SUM( traject.treden ) AS stairs' ), 
			\DB::raw( 'ROUND(SUM(traject.treden)*0.15, 2) AS special' ), 
			\DB::raw( 'ROUND( AVG( traject.tijd ) , 2 ) AS avg_time' )) );
	}

}