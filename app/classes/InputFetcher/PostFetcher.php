<?php
namespace InputFetcher;
use InputFetcher\InputFetcher;
class PostFetcher implements InputFetcher
{
	/* Static method to refactor the input to add or update a new post */
	public static function fetchInput( $input )
	{
		if ( !$input[ 'update' ] ) {
			$input[ 'id' ] = null;
		}
		$post = \Post::where( 'id', '=', $input[ 'id' ] )->get();
		$now  = new \DateTime( "now" );
		if ( null !== \Input::file( 'foto' ) ) {
			$foto                = \Input::file( 'foto' );
			$input[ 'foto' ]     = "/images/" . $now->format( 'Y-m-d' ) . $foto->getClientOriginalName();
			$input[ 'fotofile' ] = $foto;
		} else if ( $input[ 'update' ] ) {
			$input[ 'foto' ] = $post[ 0 ]->foto;
		}
		if ( null !== \Input::file( 'thumbnail' ) ) {
			$thumb                    = \Input::file( 'thumbnail' );
			$input[ 'thumbnail' ]     = "/images/" . $now->format( 'Y-m-d' ) . $thumb->getClientOriginalName();
			$input[ 'thumbnailfile' ] = $thumb;
		} else if ( $input[ 'update' ] ) {
			$input[ 'thumbnail' ] = $post[ 0 ]->thumbnail;
		}
		$input[ 'userid' ] = \Auth::user()->id;
		return $input;
	}
}