<?php
namespace InputFetcher;

interface InputFetcher {
	public static function fetchInput( $input );
}