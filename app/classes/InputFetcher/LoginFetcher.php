<?php

namespace InputFetcher;

use InputFetcher\InputFetcher;

class LoginFetcher implements InputFetcher
{
	/* Static method to reorder the given login-input */
    public static function fetchInput($input)
    {
        return array(
            'emailadres' => $input['emailadres'],
            'password' => $input['wachtwoord']
        );
        
    }
}