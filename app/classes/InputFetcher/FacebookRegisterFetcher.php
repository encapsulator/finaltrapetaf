<?php

namespace InputFetcher;

use InputFetcher\InputFetcher;

class FacebookRegisterFetcher implements InputFetcher
{
    /* decode the data
    @to-do: poor naam, voornaam solution
    */   
    public static function fetchInput($input)
    {
        
        $naam = $input["name"];
        $naam = explode(" ", $naam); // <= this is very poor (should give it a dollar)
        
        $date     = new \DateTime($input["birthday"]);
        $newInput = array(
            "email" => $input["email"],
            "voornaam" => $naam[0],
            "naam" => $naam[1],
            "woonplaats" => $input["location"]["name"],
            "groep" => $input["groep"],
            "groepid" => $input["groep"],
            "geboortedatum" => $date->format('d-m-Y'),
            "password" => $input["password"],
            "passwordCheck" => $input["password"],
            "wachtwoord" => $input["password"],
            "telefoonnummer" => null,
            "adres" => null
        );
        
        return $newInput;
    }
}