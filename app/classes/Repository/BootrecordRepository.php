<?php
namespace Repository;
class BootrecordRepository extends BaseRepository
{
	public function __construct( \Scan $scanModel )
	{
		$this->model = $scanModel;
	}
	public function make( $input )
	{
		$input      = array(
			 'unitid' => $input[ 0 ],
			'ipaddress' => $input[ 1 ] 
		);
		$bootrecord = new \Bootrecord;
		$bootrecord->make( $input );
		return $bootrecord->save();
	}
}