<?php
namespace Repository;
use Illuminate\Database\Eloquent\Model;
use Validation\RegisterUserValidator;
use Validation\UpdateUserValidator;
use Exceptions\ValidationException;
use InputFetcher\FacebookRegisterFetcher;
use InputFetcher\LoginFetcher;
class UserRepository extends BaseRepository
{
	protected $validators = array( 'register' => 'RegisterUserValidator', 'login' => 'LoginValidator', 'update' => 'UpdateUserValidator' );
	
	public function __construct( \User $userModel )
	{
		$this->model = $userModel;
	}
	public function make( $input )
	{
		$this->validator = \App::make( $this->getValidator( 'register' ) );
		$result          = parent::make( $input );
		if ( null !== $this->messageBag ) {
			throw new ValidationException( implode( "&", $this->messageBag ) );
		}
		return $result;
	}
	public function update( $input, $id )
	{
		$this->validator = \App::make( $this->getValidator( 'update' ) );
		$result          = parent::update( $id, $input );
		if ( null !== $this->messageBag ) {
			throw new ValidationException( implode( "&", $this->messageBag ) );
		}
		return $result;
	}
	public function delete( $id )
	{
		return parent::delete( $id );
	}
	public function login( $input )
	{
		$this->validator = \App::make( $this->getValidator( 'login' ) );
		$result          = parent::validate( $input );
		if ( $result ) {
			$remember = \Input::get( 'onthouden' ) == '1' ? true : false;
			$creds    = LoginFetcher::fetchInput( $input );
			if ( \Auth::attempt( $creds, $remember ) ) {
				\Session::put( 'admin', \Auth::user()->admin );
				return true;
			}
			return false;
		}
		throw new ValidationException( implode( "&", $this->messageBag ) );
	}
	public function logout( )
	{
		\Auth::logout();
	}
	/* Facebook Register via normal Request => can't put ajax event
	handler on registerbutton as this would bypass Facebook. Therefore
	I currently return a view with succes parameter */
	public function facebookRegister( )
	{
		list( $encoded_sig, $payload ) = explode( '.', $_REQUEST[ 'signed_request' ], 2 );
		$sig             = base64_decode( $encoded_sig );
		$data            = json_decode( base64_decode( $payload ), true );
		$data            = $data[ "registration" ];
		$input           = FacebookRegisterFetcher::fetchInput( $data );
		$this->validator = \App::make( $this->getValidator( 'register' ) );
		$valid           = parent::make( $input );
		if ( $valid ) {
			return true;
		}
		return false;
	}
	public function getAllGroups( )
	{
		return \DB::table( 'groep' )->orderBy( 'naam', 'asc' )->lists( 'naam', 'id' );
	}
	public function linkUserCard( $userid, $cardid )
	{
		$card              = \Kaart::where( 'id', '=', $cardid )->get();
		$card[ 0 ]->userid = $userid;
		$card[ 0 ]->save();
	}
	public function userOverview( )
	{
		$ajax = "$('a.glyphicon-remove').click(function(){ var link = $(this);
        $.ajax({ type: 'POST', cache: false, url: 'leden',
            data:({id: $(this).attr('value')}),
            dataType: 'json',
            success: function(data){
                 if(data.success == true){
                    link.closest('div').parent('div').slideUp();
                 }
            } 
        });
    });";
		return array(
			 \DB::table( 'user' )->paginate( "15" ),
			$ajax 
		);
	}
}