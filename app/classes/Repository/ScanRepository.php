<?php
namespace Repository;
class ScanRepository extends BaseRepository
{
	private $TIMELIM = 250;
	protected $validators = array( "scan" => 'ScanValidator' );
	
	public function __construct( \Scan $scanModel )
	{
		$this->model = $scanModel;
	}
	private function validateScan( $mifareid )
	{
		$res = \Kaart::where( 'mifareid', '=', $mifareid )->count();
		return $res > 0;
	}
	private function calculateTimeDifference( \DateTime $from, \DateTime $to )
	{
		$difference = $from->diff( $to );
		$minutes    = $difference->days * 24 * 60;
		$minutes += $difference->h * 60;
		$minutes += $difference->i;
		return ( $minutes * 60 ) + $difference->s;
	}
	public function make( $input )
	{
		$result = false;
		if ( $this->validateScan( $input[ 'mifareid' ] ) ) {
			$scan = new \scan;
			$scan->make( $input );
			$scan->save();
			$this->checkForTraject( $scan );
			$result = true;
		}
		return $result;
	}
	public function checkForTraject( $scan )
	{
		$mifareid      = $scan->mifareid;
		$scansFromUser = \Scan::where( 'mifareid', '=', $mifareid )->orderBy( 'datetime', 'desc' )->take( 2 )->get();
		if ( count( $scansFromUser ) == 2 && $scansFromUser[ 1 ]->unitid != $scansFromUser[ 0 ]->unitid ) {
			//There are found two latest scans, on different RFID unitid's.
			$soonest     = $scansFromUser[ 1 ];
			$latest      = $scansFromUser[ 0 ];
			$soonestTime = new \DateTime( $soonest[ 'datetime' ] );
			$soonestTime->format( 'd-m-Y H:i:s' );
			$latestTime = new \DateTime( $latest[ 'datetime' ] );
			$latestTime->format( 'd-m-Y H:i:s' );
			//calculate timedifference
			$timediffInSeconds = $this->calculateTimeDifference( $soonestTime, $latestTime );
			if ( $timediffInSeconds <= $this->TIMELIM ) {
				//time doesn't exceed unrealistic limit
				$userid        = \Kaart::where( 'mifareid', '=', $latest->mifareid )->take( 1 )->get( array(
					 'userid' 
				) );
				$soonestunitid = $soonest[ 'unitid' ];
				$latestunitid  = $latest[ 'unitid' ];
				$verdiepingv   = \DB::table( 'unit' )->where( 'unitid', '=', $soonestunitid )->take( 1 )->get( array(
					 'verdieping' 
				) );
				$verdiepingn   = \DB::table( 'unit' )->where( 'unitid', '=', $latestunitid )->take( 1 )->get( array(
					 'verdieping' 
				) );
				$verdvan       = $verdiepingv[ 0 ]->verdieping;
				$verdnaar      = $verdiepingn[ 0 ]->verdieping;
				//get hoeveelheid treden van verdiepingvan tot verdiepingnaar
				$treden        = \DB::table( 'trajectdata' )->where( 'verdiepingfrom', '=', $verdvan )->where( 'verdiepingto', '=', $verdnaar )->take( 1 )->get( array(
					 'treden' 
				) );
				$trajectdata   = array(
					 "vanunit" => $soonest[ 'unitid' ],
					"naarunit" => $latest[ 'unitid' ],
					"tijd" => $timediffInSeconds,
					"userid" => $userid[ 0 ][ 'userid' ],
					"datum" => $latestTime->format( 'Y-m-d H:i:s' ),
					"treden" => $treden[ 0 ]->treden,
					"treden_omhoog" => $verdvan > $verdnaar ? 0 : $treden[ 0 ]->treden 
				);
				$traject       = new \Traject;
				$traject->make( $trajectdata );
				$traject->save();
				parent::delete( $soonest[ 'id' ] );
				parent::delete( $latest[ 'id' ] );
			} else {
				parent::delete( $soonest[ 'id' ] );
			}
		} elseif ( count( $scansFromUser ) == 2 ) {
			parent::delete( $scansFromUser[ 1 ][ 'id' ] );
		}
	}
}