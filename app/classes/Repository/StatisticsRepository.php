<?php
namespace Repository;
class StatisticsRepository extends BaseRepository
{
	public function __construct( \Traject $trajectModel )
	{
		$this->model = $trajectModel;
	}
	public function make( $input )
	{
	}
	public function modify( $input )
	{
	}
	public function getStairsPerDayStats( )
	{
		return \DB::table( 'traject' )->select( array(
			 \DB::raw( 'DATE(datum) AS date' ),
			\DB::raw( 'SUM(treden) AS value' ) 
		) )->orderBy( 'datum', 'ASC' )->groupBy( \DB::raw( 'CAST(datum AS DATE)' ) )->get();
	}
	public function getTotalStairsUpDown( )
	{
		$omhoog = \DB::table( 'traject' )->select( array(
			 \DB::raw( 'SUM(treden_omhoog) as omhoog' ) 
		) )->get();
		$omlaag = \DB::table( 'traject' )->select( array(
			 \DB::raw( 'SUM(treden) - SUM(treden_omhoog) as omlaag' ) 
		) )->get();
		return array(
			 $omhoog,
			$omlaag 
		);
	}
	public function getFittestGroups( )
	{
		$start = new \DateTime( 'now' );
		$start->sub( new \DateInterval( 'P7D' ) );
		$now           = new \DateTime( 'now' );
		$fittestgroups = \DB::table( 'groep' )->leftJoin( 'user', 'user.groepid', '=', 'groep.id' )->leftJoin( 'traject', 'user.id', '=', 'traject.userid' )->where( 'datum', '>=', $start->format( 'Y-m-d' ) )->where( 'datum', '<', $now->format( 'Y-m-d' ) )->select( array(
			 \DB::raw( 'COUNT(traject.id) AS aantal' ),
			'groep.naam' 
		) )->groupBy( 'groep.naam' )->orderBy( 'aantal', 'desc' )->get();
		return $fittestgroups;
	}
}