<?php
namespace Repository;
use Challenge\ChallengeManager;
use Exceptions\ValidationException;
use Exceptions\ChallengeNotFoundException;
class ChallengeRepository extends BaseRepository
{
	public $challengeManager;
	public $validators = array( 'newchallenge' => "NewChallengeValidator" );
	public function __construct( \Challenge $challengeModel )
	{
		$this->model            = $challengeModel;
		$this->challengeManager = new ChallengeManager;
	}
	public function make( $input )
	{
		$this->validator = \App::make( $this->getValidator( 'newchallenge' ) );
		$foto            = \Input::file( 'foto' );
		$thumb           = \Input::file( 'thumbnail' );
		$now             = new \DateTime( 'now' );
		if ( null !== $foto ) {
			$input[ 'foto' ] = "/images/" . $now->format( 'Y-m-d' ) . $foto->getClientOriginalName();
		}
		if ( null !== $thumb ) {
			$input[ 'thumbnail' ] = "/images/" . $now->format( 'Y-m-d' ) . $foto->getClientOriginalName();
		}
		$input[ 'fotofile' ]      = $foto;
		$input[ 'thumbnailfile' ] = $thumb;
		$result                   = parent::make( $input );
		if ( null !== $foto ) {
			$foto->move( 'images', $now->format( 'Y-m-d' ) . $foto->getClientOriginalName() );
		}
		if ( null !== $thumb ) {
			$thumb->move( 'images', $now->format( 'Y-m-d' ) . $thumb->getClientOriginalName() );
		}
		if ( null !== $this->messageBag ) {
			throw new ValidationException( implode( "&", $this->messageBag ) );
		}
		return $result;
	}
	public function update( $id, $input )
	{
		$this->validator = \App::make( $this->getValidator( 'newchallenge' ) );
		$foto            = \Input::file( 'foto' );
		$thumb           = \Input::file( 'thumbnail' );
		$object          = $this->find( $id );
		$now             = new \DateTime( 'now' );
		if ( null == $foto ) {
			$input[ 'foto' ] = $object->foto;
		} else {
			$input[ 'foto' ] = "/images/" . $now->format( 'Y-m-d' ) . $foto->getClientOriginalName();
		}
		if ( null == $thumb ) {
			$input[ 'thumbnail' ] = $object->thumbnail;
		} else {
			$input[ 'thumbnail' ] = "/images/" . $now->format( 'Y-m-d' ) . $thumb->getClientOriginalName();
		}
		$input[ 'fotofile' ]      = $foto;
		$input[ 'thumbnailfile' ] = $thumb;
		$result                   = parent::update( $id, $input );
		if ( null !== $foto ) {
			$foto->move( 'images', $now->format( 'Y-m-d' ) . $foto->getClientOriginalName() );
		}
		if ( null !== $thumb ) {
			$thumb->move( 'images', $now->format( 'Y-m-d' ) . $thumb->getClientOriginalName() );
		}
		if ( null !== $this->messageBag ) {
			throw new ValidationException( implode( "&", $this->messageBag ) );
		}
		return $result;
	}
	public function adminChallengeOverview( )
	{
		$ajax = "$('a.glyphicon-remove').click(function(){ var link = $(this);
        $.ajax({ type: 'POST', cache: false, url: 'challenge',
            data:({id: $(this).attr('value')}),
            dataType: 'json',
            success: function(data){
                 if(data.success == true){
                    link.closest('div').parent('div').slideUp();
                 }
            } 
        });
    });";
		return array(
			 \DB::table( 'challenge' )->paginate( '15' ),
			$ajax 
		);
	}
	public function getSingleChallenge( $name )
	{
		$naam      = str_replace( "_", " ", $name );
		$challenge = \Challenge::where( "naam", '=', $naam )->take( 1 )->get();
		if ( count( $challenge ) == 0 ) {
			throw new ChallengeNotFoundException();
		}
		$this->challengeManager->initStrategy( $challenge[ 0 ] );
		$rankingboard      = $this->challengeManager->getRankingBoard();
		$previousIntervals = $this->challengeManager->listDoneIntervals();
		$previousIntervals = $previousIntervals == null ? "no intervals" : array_reverse( $previousIntervals );
		return array(
			 "scoreboard" => $rankingboard,
			"challenge" => $challenge[ 0 ],
			"previousIntervals" => $previousIntervals 
		);
	}
	public function getIntervalSingleChallenge( $name, $interval )
	{
		$naam      = str_replace( "_", " ", $name );
		$challenge = \Challenge::where( "naam", '=', $naam )->take( 1 )->get();
		if ( count( $challenge ) == 0 ) {
			throw new ChallengeNotFoundException();
		}
		$this->challengeManager->initStrategy( $challenge[ 0 ] );
		$rankingboard = $this->challengeManager->getRankingNthInterval( $interval );
		$dates        = $this->challengeManager->getBeginDateEndDateNthInterval( $interval );
		return array(
			 "scoreboard" => $rankingboard,
			"challenge" => $challenge[ 0 ],
			"interval" => $interval,
			"dateStart" => $dates[ 0 ]->format( "d-m-Y" ),
			"dateEnd" => $dates[ 1 ]->format( "d-m-Y" ) 
		);
	}
	public function updateChallenges( )
	{
		$challenges = \Challenge::all();
		$i          = 0;
		$f          = 0;
		foreach ( $challenges as $challenge ) {
			try {
				$this->challengeManager->initStrategy( $challenge );
				$this->challengeManager->checkForEndCurrentInterval();
				$this->challengeManager->checkForendChallenge();
				$i++;
			}
			catch ( Exception $e ) {
				$f++;
			}
		}
		return $i . " challenges updated succesfully.</br>" . $f . " failed.";
	}
	public function getOverview( )
	{
		$now                = new \DateTime( "now" );
		$activeChallenges   = \DB::table( 'challenge' )->where( 'dateend', '>', $now->format( "Y-m-d" ) )->where( 'datestart', '<=', $now->format( "Y-m-d" ) )->orderby( 'datestart', 'desc' )->paginate( '3' );
		$inactiveChallenges = \DB::table( 'challenge' )->where( 'dateend', '<=', $now->format( "Y-m-d" ) )->orderby( 'datestart', 'desc' )->paginate( '3' );
		return array(
			 "activeChallenges" => $activeChallenges,
			"inactiveChallenges" => $inactiveChallenges 
		);
	}
}