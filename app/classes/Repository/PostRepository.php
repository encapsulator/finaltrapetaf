<?php
namespace Repository;
use InputFetcher\PostFetcher;
use Exceptions\ValidationException;
use Exceptions\PostNotFoundException;
use Validation\NewPostValidator;
class PostRepository extends BaseRepository
{
	protected $validators = array( 'newpost' => 'NewPostValidator' );
	
	public function __construct( \Post $postModel )
	{
		$this->model = $postModel;
	}
	public function make( $input )
	{
		$this->validator   = \App::make( $this->getValidator( 'newpost' ) );
		$input[ 'update' ] = false;
		$input             = PostFetcher::fetchInput( $input );
		$foto              = \Input::file( 'foto' );
		$thumb             = \Input::file( 'thumbnail' );
		$now               = new \DateTime( 'now' );
		$result            = parent::make( $input );
		if ( null !== $foto ) {
			$foto->move( 'images', $now->format( 'Y-m-d' ) . $foto->getClientOriginalName() );
		}
		if ( null !== $thumb ) {
			$thumb->move( 'images', $now->format( 'Y-m-d' ) . $thumb->getClientOriginalName() );
		}
		if ( null !== $this->messageBag ) {
			throw new ValidationException( implode( "&", $this->messageBag ) );
		}
		return $result;
	}
	public function update( $id, $input )
	{
		$this->validator   = \App::make( $this->getValidator( 'newpost' ) );
		$input[ 'id' ]     = $id;
		$input[ 'update' ] = true;
		$input             = PostFetcher::fetchInput( $input );
		$foto              = \Input::file( 'foto' );
		$thumb             = \Input::file( 'thumbnail' );
		$now               = new \DateTime( 'now' );
		$result            = parent::update( $id, $input );
		if ( null !== $foto ) {
			$foto->move( 'images', $now->format( 'Y-m-d' ) . $foto->getClientOriginalName() );
		}
		if ( null !== $thumb ) {
			$thumb->move( 'images', $now->format( 'Y-m-d' ) . $thumb->getClientOriginalName() );
		}
		if ( null !== $this->messageBag ) {
			throw new ValidationException( implode( "&", $this->messageBag ) );
		}
		return $result;
	}
	public function findAll( )
	{
		$posts = \DB::table( 'posts' )->leftJoin( 'user', function( $join )
		{
			$join->on( 'posts.userid', '=', 'user.id' );
		} )->orderby( 'created_at', 'desc' )->paginate( '5', array(
			 'posts.id',
			'posts.article',
			'posts.beschrijving',
			'posts.title',
			'user.naam',
			'user.voornaam',
			'posts.foto',
			'posts.thumbnail',
			'posts.created_at',
			'posts.challenge' 
		) );
		return $posts;
	}
	public function getPost( $id )
	{
		$post = parent::find( $id );
		if ( null == $post ) {
			throw new PostNotFoundException();
		}
		$author = \DB::table( 'user' )->join( 'posts', 'user.id', '=', 'posts.userid' )->where( 'posts.id', '=', $id )->get( array(
			 'naam',
			'voornaam' 
		) );
		return array(
			 'post' => $post,
			'author' => $author 
		);
	}
	public function postOverview( )
	{
		$ajax = "$('a.glyphicon-remove').click(function(){ var link = $(this);
        $.ajax({ type: 'POST', cache: false, url: 'post',
            data:({id: $(this).attr('value')}),
            dataType: 'json',
            success: function(data){
                 if(data.success == true){
                    link.closest('div').parent('div').slideUp();
                 }
            } 
        });
    });";
		return array(
			 \DB::table( 'posts' )->paginate( "15" ),
			$ajax 
		);
	}
}