<?php
namespace Repository;
class TrajectRepository extends BaseRepository
{
	public function __construct( \Traject $trajectModel )
	{
		$this->model = $trajectModel;
	}
	public function make( $input )
	{
	}
	public function modify( $input )
	{
	}
	public function getLeaderboard( $orderBy = 'amount', $direction = 'DESC' )
	{
		$leaderboard = \DB::table( 'user' )->join( 'traject', 'user.id', '=', 'traject.userid' )->join( 'groep', 'user.groepid', '=', 'groep.id' )->groupBy( 'user.naam', 'user.voornaam' )->orderBy( $orderBy, $direction )->get( array(
			 \DB::raw( 'CONCAT(user.voornaam, " ", user.naam) AS username' ),
			"groep.naam AS groepnaam",
			\DB::raw( 'COUNT(traject.userid) AS amount' ),
			\DB::raw( 'ROUND(AVG(traject.tijd), 2) AS avg_time' ),
			\DB::raw( 'SUM(treden) AS stairs' ),
			\DB::raw( 'ROUND((SUM(treden)*0.15),2) AS cal' ) 
		) );
		return $leaderboard;
	}
	public function changeOrder( $input )
	{
		$orderBy   = $input[ 'orderby' ];
		$direction = $input[ 'ordertype' ];
		return $this->getLeaderboard( $orderBy, $direction );
	}
	//history trajects
	public function findAll( )
	{
		$movements = \DB::table( 'traject' )->join( 'user', 'traject.userid', '=', 'user.id' )->join( 'unit', 'traject.vanunitid', '=', 'unit.unitid' )->join( 'unit AS unittwee', 'unittwee.unitid', '=', 'traject.naarunitid' )->orderBy( 'datum', 'desc' )->get( array(
			 \DB::raw( 'CONCAT(user.voornaam, " ", user.naam) AS username' ),
			'traject.datum AS datum',
			'traject.tijd AS tijd',
			'unit.verdieping AS verdiepingvan',
			'unittwee.verdieping AS verdiepingto' 
		) );
		$results   = \Paginator::make( $movements, count( $movements ), 10 );
		return $results;
	}
}