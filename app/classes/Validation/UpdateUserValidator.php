<?php

namespace Validation;

class UpdateUserValidator extends BaseModelValidator{
 	protected $rules = array(
        "naam" => "required", 
	"voornaam" => "required", 
	"emailadres" => "required|email", 
	 "groep" => "required"
   	);

    	protected $messages = array( 
	'emailadres.required'                        =>  'Het e-mailadres is verplicht.',
        'emailadres.email'                           =>  'Het e-mailadres is ongeldig.',
        
        'voornaam.required'                    =>  'Uw voornaam is verplicht.',

        'naam.required'                     	=>  'Uw achternaam is verplicht.',

	   'groep.required'			=> 'Uw groep is niet aangegeven.'

	);

}
