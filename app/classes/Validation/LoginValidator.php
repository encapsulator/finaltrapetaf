<?php

namespace Validation;

class LoginValidator extends BaseModelValidator {

	protected $rules = array(
		"emailadres" => "required|email", 
		"wachtwoord" => "required|min:6");

	protected $messages = array(
		"emailadres.required" => "U heeft geen e-mailadres opgegeven.",
		"emailadres.email"	  => "U heeft geen geldig e-mailadres opgegeven.",
		"wachtwoord.required" => "U heeft geen wachtwoord opgegeven.",
		"wachtwoord.min"	  => "Uw wachtwoord moet minstens 6 tekens bevatten." 
	);

}