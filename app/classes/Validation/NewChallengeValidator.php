<?php

namespace Validation;

class NewChallengeValidator extends BaseModelValidator {
	protected $rules = [
		"naam"=>"required|min:10",
		"datestart"=>"required|date",
		"dateend"=>"required|date",
		"beschrijving"=>"required|between: 175, 1000",
		"foto"=>"required",
		"fotofile"=>"image|image-size:400,300",
		"thumbnailfile"=>"image|image-size:200,150",
		"interval"=>"alpha_num",
		"beschrijvingNew"=>"between:175,1000",
		"beschrijvingEnd"=>"between:175, 1000",
		"beschrijvingNewInterval"=>"between: 175,1000",
	];
	protected $messages = [
		"naam.required"=>"U heeft geen naam opgegeven voor de challenge.",
		"naam.min"	   =>"De naam dient minstens 10 karakters te bevatten.",	
		"datestart.required"=>"U heeft geen startdatum opgegeven.",
		"datestart.date"=>"De startdatum is geen geldige datum.",
		"dateend.required"=>"U heeft geen einddatum opgegeven",
		"dateend.date"=>"De einddatum is geen geldige datum.",
		"beschrijving.required"=>"U heeft geen beschrijving opgegeven.",
		"beschrijving.between"=>"De beschrijving dient tussen de 175 en de 1000 karakters te zijn.",
		"foto.required"=>"U heeft geen foto gekozen.",
		"fotofile.image"=>"Het gekozen bestand is geen foto.",
		"fotofile.image-size"=>"Uw foto dient 400 x 300 px te zijn.",
		"thumbnailfile.image"=>"De gekozen thumbnail is geen foto",
		"thumbnailfile.image-size"=>"Uw thumbnail dient 200 x 150 px te zijn.",
		"interval.alpha_num" => "Het opgegeven interval is niet numeriek.",
		"beschrijvingNew.between"=>"De beschrijving van de post die automatisch wordt gegenereerd bij een nieuwe challenge dient tussen 175 en 1000 karakters te zijn.",
		"beschrijvingEnd.between"=>"De beschrijving van de post die automatisch wordt gegenereerd bij het einde van een challenge dient tussen 175 en 1000 karakters te zijn.",
		"beschrijvingNewInterval.between"=>"De beschrijving van de post die automatisch wordt gegenereerd bij het begin van een nieuw interval dient tussen 175 en 1000 karakters te zijn."
	];
}