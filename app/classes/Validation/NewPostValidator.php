<?php

namespace Validation;

class NewPostValidator extends BaseModelValidator {
	protected $rules = [
		"title"	        => "required|min:10",
		"beschrijving"  => "required|between:175,1000" ,
		"foto"          => "required",
		"fotofile" 	    => "image|image-size:400,300",
		"thumbnailfile" => "image|image-size:200,150"
	];
	protected $messages = [
		"title.required"           => "U heeft geen titel opgegeven.",
		"title.min"		           => "Een titel moet bestaan uit minimum 10 tekens.",
		"beschrijving.required"    => "U heeft geen beschrijving opgegeven.",
		"beschrijving.between"	   => "Een beschrijving moet uit minimaal 175 en maximaal 1000 tekens bestaan.",
		"foto.required"			   => "U heeft geen foto gekozen.",
		"fotofile.image"		   => "Uw foto is geen afbeelding.",
		"fotofile.image-size"	   => "Uw foto moet 400x300 pixels zijn.",
		"thumbnailfile.image"	   => "Het thumbnailveld bevat geen afbeelding.",
		"thumbnailfile.image-size" => "Uw thumbnail moet 200x150 pixels zijn."
	];
}