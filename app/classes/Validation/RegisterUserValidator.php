<?php

namespace Validation;

class RegisterUserValidator extends BaseModelValidator{
 	protected $rules = array(
        "naam" => "required", 
	"voornaam" => "required", 
	"email" => "required|email|unique:user,emailadres", 
	"password" => "required|min:6", 
	"passwordCheck" => "required|same:password",
	 "groep" => "required"
   	);

    	protected $messages = array( 
	'email.required'                        =>  'Het e-mailadres is verplicht.',
        'email.email'                           =>  'Het e-mailadres is ongeldig.',
        'email.unique'                          =>  'Het e-mailadres is reeds geregistreerd.',

        'password.required'                     =>  'Het wachtwoord is verplicht.',
        'password.min'                          =>  'Het wachtwoord dient een minimale lengte van 6 tekens te hebben.',

        'voornaam.required'                    =>  'Uw voornaam is verplicht.',

        'naam.required'                     	=>  'Uw achternaam is verplicht.',
        'passwordCheck.required'                          =>  'Het controlewachtwoord is verplicht.',
	   'groep.required'			=> 'Uw groep is niet aangegeven.',
	   'passwordCheck.same'			=> 'Het controlewachtwoord is niet gelijk aan het origineel.'
	);

}
