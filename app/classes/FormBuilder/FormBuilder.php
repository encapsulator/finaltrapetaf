<?php

namespace FormBuilder;

abstract class FormBuilder {
	
	abstract function build($hasFile);
	abstract function reset();

}