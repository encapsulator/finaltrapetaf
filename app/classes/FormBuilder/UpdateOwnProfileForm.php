<?php

namespace FormBuilder;

class UpdateOwnProfileForm extends FormDirector {

	public function __construct( $formBuilder, $id ){
		$user = \User::where('id','=', $id)->get()[0];
		$groups = \DB::table('groep')->lists('naam', 'id');

		$currentGroup = \DB::table('groep')->where('id', '=', $user->groepid)->take(1)->get();
		
		$adminopties = [0=>"Gebruiker",1=>"Admin"];

		$dt = new \DateTime($user->geboortedatum);
		
		$this->setBuilder($formBuilder);
		$this->hasFile = false;
	
		$this->formData =[
			[
				"type"=>"select", "id"=>"groepid", "name"=>"groep", "value"=>"Groep* ",
				"options"=>$groups, "selected"=>$currentGroup[0]->id
			],[
				"type"=>"textInput","id"=>"naamid",
				"name"=>"naam","value" => "Naam*", "currentVal"=>$user->naam
			],[
				"type"=>"textInput","id"=>'voornaamid',
				"name"=>"voornaam","value"=>"Voornaam*", "currentVal"=>$user->voornaam
			],[
				"type"=>"textInput","id"=>"adresid",
				"name"=>"adres","value" => "Adres", "currentVal"=>$user->adres
			],[
				"type"=>"textInput","id"=>'woonplaatsid',
				"name"=>"woonplaats","value"=>"Woonplaats", "currentVal"=>$user->woonplaats
			],[
				"type"=>"textInput","id"=>"emailadresid",
				"name"=>"emailadres","value" => "Emailadres*", "currentVal"=>$user->emailadres
			],[
				"type"=>"textInput","id"=>'telefoonnummerid',
				"name"=>"telefoonnummer","value"=>"Telefoonnummer", "currentVal"=>$user->telefoonnummer
			],[
				"type"=>"textInput","id"=>"passwordid",
				"name"=>"password","value" => "Wijzig wachtwoord", "currentVal"=>''
			],[
				"type"=>"textInput","id"=>'geboortedatumid',
				"name"=>"geboortedatum","value"=>"Geboortedatum", "currentVal"=>$user->geboortedatum == "0000-00-00"?"0000-00-00": $dt->format('d-m-Y')
			],[
				"type"=>"submit", "value"=>"Verzenden"
			]
		];
	}

}

