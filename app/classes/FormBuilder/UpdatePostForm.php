<?php

namespace FormBuilder;

class UpdatePostForm extends FormDirector {

	public function __construct($formBuilder, $id){
		$post = \Post::where('id', '=', $id)->get()[0];
		$this->setBuilder($formBuilder);
		$this->hasFile = true;
		$this->formData = 
		[
			[
				"type"=>"textInput","id"=>"titleid",
				"name"=>"title","value" => "Title*", "currentVal"=>$post->title
			],[
				"type"=>"textarea","id"=>'discription',
				"name"=>"beschrijving","value"=>"Discription*", "currentVal"=>$post->beschrijving
			],[
				"type" => "updatefotofield", "id"=>"thumbnailid",
				"name"=>"thumbnail", "value"=>"Thumbnail (200x150)", "currentVal" => $post->thumbnail,
				"style"=>""
			],[
				"type"=>"textarea", "id"=>"articleid",
				"name"=>"article", "value"=>"Article", "currentVal" => $post->article
			],[
				"type"=>"updatefotofield","id" => "fotoid",
				"name" => "foto","value" => "Photo* (400x300)", "currentVal" => $post->foto,
				"style"=>""
			],[
				"type"=>"submit", "value"=>"Verzenden"
			]
		];
	}

}