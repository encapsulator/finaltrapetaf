<?php
namespace FormBuilder;

class FormDirector {
	
	protected $formBuilder;
	protected $formData;
	protected $hasFile; 

	public function __construct($formbuilder, $hasFile){
		$this->setBuilder($formbuilder);
	}
	
	protected function setBuilder($formbuilder){
		$this->formBuilder = $formbuilder;
	}

	public function build(){
		foreach($this->formData as $input){
			if($input['type'] == "textInput"){
				$this->formBuilder->addTextInput($input['id'], $input['name'], $input['value'], $input['currentVal']);
			}
			if($input['type'] == "fileInput"){
				$this->formBuilder->addFileField($input['id'], $input['name'], $input['value'], $input['currentVal']);
			}
			if($input['type']=="updatefotofield"){
				$this->formBuilder->addUpdateFotoField($input['id'], $input['name'], $input['value'], $input['currentVal'], $input['style']);
			}
			if($input['type']=="submit"){
				$this->formBuilder->addSubmit($input['value']);
			}
			if($input['type']=="select"){
				$this->formBuilder->addSelect($input['id'], $input['name'], $input['value'], $input['options'], $input['selected']);
			}
			if($input['type']=="textarea"){
				$this->formBuilder->addTextarea($input['id'], $input['name'], $input['value'], $input['currentVal']);
			}
			if($input['type']=="checkbox"){
				$this->formBuilder->addCheckbox($input['id'], $input['name'], $input['value'], $input['selected']);
			}
		}
		$form = $this->formBuilder->build($this->hasFile);
		return ["form"=>$form->getForm(), "ajax"=>$this->getAjax()];
	}

	private function getAjax(){
		return $this->formBuilder->getAjax();
	}
	
}