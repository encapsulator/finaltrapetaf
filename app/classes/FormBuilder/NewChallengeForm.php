<?php

namespace FormBuilder;

class NewChallengeForm extends FormDirector {
	public function __construct( $formBuilder ){

		$challengeOptions = [
			"fastestAvarageTimeStrategy" => "Snelste gemiddelde tijd",
			"fastestTimePerStairDownStrategy" => "Snelste gemiddelde tijd per trap naar beneden",
			"fastestTimePerStairStrategy" => "Snelste gemiddelde tijd per trap",
			"fastestTimePerStairUpStrategy" => "Snelste gemiddelde tijd per trap naar boven",
			"mostCaloriesBurnedStrategy" => "Meeste calorieën verbrand",
			"mostStairsDownStrategy" => "Meest aantal trappen naar beneden", 
			"mostStairsStrategy" => "Meest aantal trappen", 
			"mostStairsUpPerStairStrategy" => "Meest aantal trappen omhoog per trap",
			"mostStairsUpStrategy" => "Meest aantal trappen omhoog",
			"mostTimesDownStrategy" => "Meest aantal keren naar beneden",
			"mostTimesTakenStairsStrategy" => "Meest aantal keer de trappen genomen",
			"mostTimesUpStrategy" => "Meest aantal keren naar omhoog"
		];

		$groepen = \DB::table('groep')->lists('naam', 'id');
		$geengroep = [0=>"Alle groepen"];
		$groupsoptions = array_merge($geengroep, $groepen);
		asort($challengeOptions);

		$this->setBuilder( $formBuilder );
		$this->hasFile = true;
		$this->formData = [[
			"type"=>"textInput", "id"=>'naamid', "name"=>"naam",
			"value"=>"Naam* ", "currentVal"=>""
		],[
			"type"=>"textInput", "id"=>"datestartid", "name"=>"datestart",
			"value"=>"Startdatum* (dd-mm-yyyy) ", "currentVal"=>""
		],[
			"type"=>"textInput", "id"=>"dateendid", "name"=>"dateend",
			"value"=>"Einddatum* (dd-mm-yyyy)", "currentVal"=>""
		],[
			"type"=>"textarea", "id"=>"beschrijvingid", "name"=>"beschrijving", 
			"value"=>"Beschrijving* ", "currentVal"=>""
		],[
			"type"=>"fileInput", "id"=>"fotoid", "name"=>"foto", 
			"value"=>"Foto* (400x300 px)", "currentVal"=>""
		],[
			"type"=>"fileInput", "id"=>"thumbnailid", "name"=>"thumbnail", 
			"value"=>"Thumbnail (200x150 px)", "currentVal"=>""
		],[
			"type"=>"select", "id"=>"orderingid", "name"=>"ordering", 
			"value"=>"Soort* ", "options"=>$challengeOptions, "selected"=>""
		],[
			"type"=>"checkbox", "id"=>"reverseid", "name"=>"reverse", 
			"value"=>"Omkeren* ", "selected"=>false
		],[
			"type"=>"select", "id"=>"idgroep", "name"=>"groepid", 
			"value"=>"Specifieke groep", "options"=>$groupsoptions, "selected"=>"Alle groepen"
		],[
			"type"=>"checkbox", "id"=>"wederkerendid", "name"=>"wederkerend", 
			"value"=>"Wederkerend* ", "selected"=>false
		],[
			"type"=>"textInput", "id"=>"intervalid", "name"=>"interval", 
			"value"=>"Tijdsinterval in dagen (* indien wederkerend)", "currentVal"=>""
		],[
			"type"=>"textarea", "id"=>"beschrijvingNew", "name"=>"beschrijvingNew", 
			"value"=>"Automatische post start challenge", "currentVal"=>""
		],[
			"type"=>"textarea", "id"=>"beschrijvingNewInterval", "name"=>"beschrijvingNewInterval", 
			"value"=>"Automatische post start nieuw interval", "currentVal"=>""
		],[
			"type"=>"textarea", "id"=>"beschrijvingEnd", "name"=>"beschrijvingEnd", 
			"value"=>"Automatische post einde challenge", "currentVal"=>""
		],[
			"type"=>"submit", "value"=>"Verzenden"
		]
		];
	}
}