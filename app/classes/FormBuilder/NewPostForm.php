<?php

namespace FormBuilder;

class NewPostForm extends FormDirector {

	public function __construct($formBuilder){
		$this->setBuilder($formBuilder);
		$this->hasFile = true;
		$this->formData = 
		[
			[
				"type"=>"textInput","id"=>"titleid",
				"name"=>"title","value" => "Title*", "currentVal"=>""
			],[
				"type"=>"textarea","id"=>'discription',
				"name"=>"beschrijving","value"=>"Discription*","currentVal"=>""
			],[
				"type" => "fileInput", "id"=>"thumbnailid",
				"name"=>"thumbnail", "value"=>"Thumbnail (150x200)", "currentVal"=>""
			],[
				"type"=>"textarea", "id"=>"articleid",
				"name"=>"article", "value"=>"Article", "currentVal"=>""
			],[
				"type"=>"fileInput","id" => "fotoid",
				"name" => "foto", "value" => "Photo* (300x400)", "currentVal"=>""
			],[
				"type"=>"submit", "value"=>"Verzenden"
			]];	
	}

}