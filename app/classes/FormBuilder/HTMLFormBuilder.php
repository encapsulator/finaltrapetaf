<?php
namespace FormBuilder;
use FormBuilder\FormBuilder;
use FormBuilder\HTMLForm;
class HTMLFormBuilder extends FormBuilder
{
	private $elements;
	private $formid;
	private $route;
	public $hasFile;
	public function __construct( $formid, $route )
	{
		$this->formid   = $formid;
		$this->route    = $route;
		$this->elements = array( );
	}
	public function reset( )
	{
		$this->elements = array( );
	}
	public function getFormElements( )
	{
		return $this->elements;
	}
	public function build( $hasFile )
	{
		$this->hasFile = $hasFile;
		return new HTMLForm( $this );
	}
	public function getAjax( )
	{
		$result = "$('#" . $this->formid . "').submit( function() {
				var formData = new FormData($('#" . $this->formid . "')[0]);

				if($('input:file[name=foto]').length > 0){
					var filefoto = $('input:file[name=foto]')[0].files[0];
					var filethumb = $('input:file[name=thumbnail]')[0].files[0];
				
					if(filefoto != null){
						formData.append('foto', filefoto);
					}
					if(filethumb != null){
						formData.append('thumbnail', filethumb);
					}
				}
				$.ajax({
                    type: 'POST',
                    cache: false,
                    url: '" . $this->route . "',
                    data: formData,
                     processData: false,
                     contentType: false,
                    dataType: 'json',
                    beforeSend: function(){
                    	$('#validation-errors').removeClass('alert-success');
                    	$('#validation-errors').addClass('alert-danger');
                    	$('#validation-errors').html(\"";
		$result .= "<a class='close' href='#'>×</a> \")";
		$result .= "},
                	success: function(data) {
                		window.scrollTo(0, 0);
                		if(data.success == false){
                			var arr = data.errors;
                			
                			$.each(arr, function(index, value){
                				if(value.length!=0){
                					$('#validation-errors').append('<p>'+value+'</p>');
                                }
                			});
                			$('#validation-errors').slideDown();
                		} else {
                			$('#validation-errors').toggleClass('alert-danger');
                        	$('#validation-errors').toggleClass('alert-success');
                        	$('#validation-errors').append('<p>De gegevens werden succesvol verwerkt!</p>');
                        	$('#validation-errors').slideDown();
                		}
                	},
                	error: function(xhr, textStatus, thrownError){
                		$('#validation-errors').append('<p>Er ging iets mis, probeer later opnieuw.</p>');
                		$('#validation-errors').slideDown();
                	}

                });
                return false;		
        });

		 $('body').on('click', 'a.close', function() {
	  $('#validation-errors').slideUp();
		});";
		return $result;
	}
	public function getFormTag( )
	{
		return "<div id='formulier'>" . \Form::open( array(
			 'url' => $this->route,
			'method' => 'post',
			'id' => $this->formid,
			'files' => $this->hasFile 
		) );
	}
	public function closeFormTag( )
	{
		return \Form::close() . "</div>";
	}
	private function addLabel( $id, $value )
	{
		return \Form::label( $id, $value );
	}
	public function addTextInput( $id, $name, $value, $currentVal )
	{
		$input             = \Form::text( $name, $currentVal, array(
			 'id' => $id,
			'class' => "form-control",
			"placeholder" => $value 
		) );
		$this->elements[ ] = array(
			 "label" => $this->addLabel( $id, $value ),
			"input" => $input 
		);
	}
	public function addTextarea( $id, $name, $value, $currentVal )
	{
		$input             = \Form::textarea( $name, $currentVal, array(
			 'id' => $id,
			'class' => "form-control",
			"placeholder" => $value 
		) );
		$this->elements[ ] = array(
			 "label" => $this->addLabel( $id, $value ),
			"input" => $input 
		);
	}
	public function addFileField( $id, $name, $value, $currentVal )
	{
		$input             = $value . " : " . \Form::file( $name, array(
			 'id' => $id,
			'class' => "form-control" 
		) );
		$this->elements[ ] = array(
			 "label" => $this->addLabel( $id, $value ),
			"input" => $input 
		);
	}
	public function addCheckbox( $id, $name, $value, $selected )
	{
		$input             = "<p>" . $value . ' :  ' . \Form::checkbox( $name, 1, $selected, array(
			 'id' => $id 
		) ) . "</p>";
		$this->elements[ ] = array(
			 "label" => $this->addLabel( $id, $value ),
			"input" => $input 
		);
	}
	public function addUpdateFotoField( $id, $name, $value, $currentVal, $style )
	{
		$input = $value . " :<br/>";
		$input .= "<img class='" . $id . "' src='" . \URL::asset( $currentVal ) . "' alt='" . $value . "' style ='" . $style . "' /><br/>";
		$input .= \Form::file( $name, array(
			 'id' => $id,
			"class" => "form-control" 
		) );
		$this->elements[ ] = array(
			 "label" => $this->addLabel( $id, $value ),
			"input" => $input 
		);
	}
	public function addSelect( $id, $name, $value, $options, $selected )
	{
		$input             = $value . " :" . \Form::select( $name, $options, $selected, array(
			 'id' => $id,
			'class' => "form-control" 
		) );
		$this->elements[ ] = array(
			 "label" => $this->addLabel( $id, $value ),
			"input" => $input 
		);
	}
	public function addSubmit( $val )
	{
		$input             = \Form::submit( $val, array(
			 'class' => 'btn btn-success' 
		) );
		$this->elements[ ] = array(
			 "label" => '',
			"input" => $input 
		);
	}
}