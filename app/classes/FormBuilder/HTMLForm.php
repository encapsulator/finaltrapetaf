<?php
namespace FormBuilder;
use FormBuilder\HTMLFormBuilder;
class HTMLForm
{
	private $formText;
	static function createBuilder( $formid, $route )
	{
		return new HTMLFormBuilder( $formid, $route );
	}
	public function addElement( $label, $element )
	{
		$this->formText .= $element . "\n";
	}
	public function __construct( HTMLFormBuilder $b )
	{
		$elements       = $b->getFormElements();
		$this->formText = $b->getFormTag();
		$i              = 0;
		$j              = count( $elements );
		foreach ( $elements as $element ) {
			$this->formText .= $this->addElement( $element[ 'label' ], $element[ 'input' ] );
			$i++;
			if ( $i == $j - 1 ) {
				$this->formText .= "<p>Velden met een * zijn verplicht.</p>";
			}
		}
		$this->formText .= $b->closeFormTag();
	}
	public function getForm( )
	{
		return $this->formText;
	}
}