<?php

namespace FormBuilder;

class LinkUserCardForm extends FormDirector {
	
	public function __construct( $formBuilder ){

		$usersNoCard = \DB::table('user')->leftjoin('kaart', 'user.id', '=', 'kaart.userid')
						->where('kaart.userid', '=', null)->
						select('user.id AS userid', \DB::raw('CONCAT(user.voornaam, " ", user.naam) AS username'))
						->lists('username', 'userid');

		$cardsNoUser = \DB::table('kaart')->where('userid', '=', null)->lists('kaart.id', 'id');				
	
		$this->setBuilder( $formBuilder );

		$this->hasFile = false;

		$this->formData = [[
			"type"=>'select', "id" => "usersnocard", "name"=>"user",
			"value"=>"User*", "options"=>$usersNoCard, "selected"=>''
		],[
			"type"=>'select', "id"=>"cardsnouser", "name"=>"kaart",
			"value"=>"Kaart* ", "options"=>$cardsNoUser, "selected"=>''
		],[
			"type"=>"submit", "value"=>"Verzenden"
		]];
	}

}