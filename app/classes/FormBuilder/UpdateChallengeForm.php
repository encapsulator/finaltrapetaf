<?php

namespace FormBuilder;

class UpdateChallengeForm extends FormDirector {
	public function __construct( $formBuilder, $id ){

		$challenge = \DB::table('challenge')->where('id', '=', $id)->get();
		$challenge = $challenge[0];

		$challengeOptions = [
			"fastestAvarageTimeStrategy" => "Snelste gemiddelde tijd",
			"fastestTimePerStairDownStrategy" => "Snelste gemiddelde tijd per trap naar beneden",
			"fastestTimePerStairStrategy" => "Snelste gemiddelde tijd per trap",
			"fastestTimePerStairUpStrategy" => "Snelste gemiddelde tijd per trap naar boven",
			"mostCaloriesBurnedStrategy" => "Meeste calorieën verbrand",
			"mostStairsDownStrategy" => "Meest aantal trappen naar beneden", 
			"mostStairsStrategy" => "Meest aantal trappen", 
			"mostStairsUpPerStairStrategy" => "Meest aantal trappen omhoog per trap",
			"mostStairsUpStrategy" => "Meest aantal trappen omhoog",
			"mostTimesDownStrategy" => "Meest aantal keren naar beneden",
			"mostTimesTakenStairsStrategy" => "Meest aantal keer de trappen genomen",
			"mostTimesUpStrategy" => "Meest aantal keren naar omhoog"
		];

		$startdate = new \DateTime($challenge->datestart);
		$enddate = new \DateTime($challenge->dateend);
		$intervalbase = new \DateTime('0000-00-00');
		
		$daysinterval = $challenge->timeInterval;

		$groepen = \DB::table('groep')->lists('naam', 'id');
		$geengroep = [0=>"Alle groepen"];
		$groupsoptions = array_merge($geengroep, $groepen);
		asort($challengeOptions);

		$this->setBuilder( $formBuilder );
		$this->hasFile = true;
		$this->formData = [[
			"type"=>"textInput", "id"=>'naamid', "name"=>"naam",
			"value"=>"Naam* ", "currentVal"=>$challenge->naam
		],[
			"type"=>"textInput", "id"=>"datestartid", "name"=>"datestart",
			"value"=>"Startdatum* (dd-mm-yyyy) ", "currentVal"=>$startdate->format('d-m-Y')
		],[
			"type"=>"textInput", "id"=>"dateendid", "name"=>"dateend",
			"value"=>"Einddatum* (dd-mm-yyyy)", "currentVal"=>$enddate->format('d-m-Y')
		],[
			"type"=>"textarea", "id"=>"beschrijvingid", "name"=>"beschrijving", 
			"value"=>"Beschrijving* ", "currentVal"=>$challenge->beschrijving
		],[
			"type"=>"updatefotofield", "id"=>"fotoid", "name"=>"foto", 
			"value"=>"Foto* (400x300 px)", "currentVal"=>$challenge->foto, "style"=>""
		],[
			"type"=>$challenge->thumbnail!=null?"updatefotofield":"fileInput", "id"=>"thumbnailid", "name"=>"thumbnail", 
			"value"=>"Thumbnail (200x150 px)", "currentVal"=>$challenge->thumbnail, "style"=>''1111
		],[
			"type"=>"select", "id"=>"orderingid", "name"=>"ordering", 
			"value"=>"Soort* ", "options"=>$challengeOptions, "selected"=>$challenge->ordering
		],[
			"type"=>"checkbox", "id"=>"reverseid", "name"=>"reverse", 
			"value"=>"Omkeren* ", "selected"=>$challenge->reverse==1
		],[
			"type"=>"select", "id"=>"idgroep", "name"=>"groepid", 
			"value"=>"Specifieke groep*", "options"=>$groupsoptions, "selected"=>$challenge->groepid==0?"Alle groepen":$challenge->groepid
		],[
			"type"=>"checkbox", "id"=>"wederkerendid", "name"=>"wederkerend", 
			"value"=>"Wederkerend* ", "selected"=>$challenge->timeInterval != null
		],[
			"type"=>"textInput", "id"=>"intervalid", "name"=>"interval", 
			"value"=>"Tijdsinterval in dagen (* wanneer wederkerend) ", "currentVal"=>$daysinterval
		],[
			"type"=>"textarea", "id"=>"beschrijvingNew", "name"=>"beschrijvingNew", 
			"value"=>"Automatische post start challenge", "currentVal"=>$challenge->beschrijvingNew
		],
[			"type"=>"textarea", "id"=>"beschrijvingNewInterval", "name"=>"beschrijvingNewInterval", 
			"value"=>"Automatische post start nieuw interval", "currentVal"=>$challenge->beschrijvingNewInterval
		],[
			"type"=>"textarea", "id"=>"beschrijvingEnd", "name"=>"beschrijvingEnd", 
			"value"=>"Automatische post einde challenge", "currentVal"=>$challenge->beschrijvingEnd
		],[
			"type"=>"submit", "value"=>"Verzenden"
		]
		];
	}
}