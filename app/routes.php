<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/
//indexpagina


//basic pages / no login
Route::group(array('before' => ''), function()
{
	Route::get( '/', 'PostController@getOverview' );

	//loginpage
	Route::get('login', 'UserController@getLoginPage');
	//login post
	Route::post('login', array('before'=>'', 'as' => 'login', 'uses'=>'UserController@login'));

	//Aanmelden: get available cards & groepen formulier
	Route::get('aanmelden', 'UserController@getRegisterPage');

	//Post single
	Route::get('post/{id}', 'PostController@getSingle' );

	//History all valid trajects
	Route::get("history", 'TrajectController@getHistory');

	//Post aanmeldformulier
	Route::post('aanmelden', array('before'=>'csrf', 'as' => 'aanmelden', 'uses'=>'UserController@addUser'));

	//Post aanmelden via facebook request
	Route::post('facebookregister', array( 'as' => 'facebookRegister', 'uses'=>'UserController@facebookRegister'));

	//Scoreboard
	Route::get('leaderboard', "TrajectController@getLeaderboard");

	//Scoreboard change ordertype
	Route::post('leaderboard', "TrajectController@changeLeaderboardOrder");

	//statistics

	Route::get('statistics', "StatisticsController@getStatistics");

});

//Challenges
Route::group(array( 'prefix'=>'challenges' ), function()
{
	Route::get("", "ChallengeController@getOverview");

	//Single challenge
	Route::get("{name}", "ChallengeController@getSingleChallenge");

	//Get Nth interval single challenge
	Route::get('{name}/{interval}', "ChallengeController@getIntervalSingleChallenge");

});

//logout

Route::group(array('before' => 'auth'), function()
{
	Route::get('logout', 'UserController@logout');
});

//RFID
Route::group(array('prefix'=>'pilog'), function(){
	//registreer nieuwe scan
	Route::get('scan/{mifareid}/{unitid}', 'ScanController@scan');

	//rpi & arduino bootrecord update
	Route::get('boot/{ipaddress}/{unitid}', 'ScanController@boot');
});


//admin
Route::group(array('prefix' => 'admin', 'before' => array('auth|admin')), function()
{

	Route::get('post', 'AdminController@postsOverview');

	Route::get('post/new', 'AdminController@buildPostForm');

	Route::post('post/new', 'AdminController@newPostSubmit');

	Route::get('post/{id}', "AdminController@buildUpdatePostForm");

	Route::post('post/{id}', "AdminController@updatePostSubmit");

	Route::post('post', "AdminController@deletePost");

	Route::get('batch', "AdminController@buildLinkUserCardForm");

	Route::post('batch', "AdminController@linkUserCard");

	Route::get('leden', "AdminController@membersOverview");

	Route::post('leden', "AdminController@deleteLid");

	Route::get('leden/{id}', "AdminController@updateMemberForm");

	Route::get('challenge', "AdminController@challengeOverview");

	Route::post("leden/{id}", "AdminController@updateProfile");

	Route::post('challenge', "AdminController@deleteChallenge");

	Route::get('challenge/new', "AdminController@buildNewChallengeForm");

	Route::post('challenge/new', "AdminController@postNewChallenge");	

	Route::get('challenge/{id}', "AdminController@buildUpdateChallengeForm");

	Route::post('challenge/{id}', 'AdminController@postUpdateChallenge');
});
//admin
Route::group(array('prefix' => 'admin', 'before' => array('auth')), function()
{
	Route::get('profiel', "AdminController@buildProfileForm");

	Route::post("profiel", "AdminController@updateOwnProfile");

	Route::get('', 'AdminController@main');
});
//Update all challenges DB-job
Route::get("NqOlP785/update/challenges","ChallengeController@updateChallenges");

