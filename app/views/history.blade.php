@extends('layout')
<?php View::share('pagetitle', 'pagetitle'); ?>
<?php View::share('pagebeschrijving', 'pagebeschrijving'); ?>
@section('content')
<main>
    <article class="row">	
    	 <h2>History</h2>  
   		<div class="col-lg-12">
		<div class="row">
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
				Nr.
			</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
				Name
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
				Datum/tijd
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
				Van verdieping 
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
				Naar verdieping
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 visible-lg head">
				Tijd
		</div>
		</div>
		<div id="scoredata">
		<?php $i=1 ?>
		@foreach($bewegingen as $move)
			@if($i >= $bewegingen->getFrom() && $i <= $bewegingen->getTo())
			<div class="row data">
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				<?php echo $i ?>
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				{{$move->username}}
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				<?php $datetime = new Datetime($move->datum); echo $datetime->format('d-m-Y H:i:s')?>
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				{{$move->verdiepingvan}} 
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				{{$move->verdiepingto}}
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 visible-lg">
				{{$move->tijd}}
			</div>
			</div>			
			@endif
			<?php $i++; ?>
		@endforeach
		</div>
		<p>{{$bewegingen->links()}}</p>
		</div>	
	</article>
<script type="text/javascript" src="{{URL::asset('js/ajaxCalls.js')}}"></script>
</main>
@stop