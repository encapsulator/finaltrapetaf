@extends('layout')
<?php View::share('pagetitle', 'pagetitle'); ?>
<?php View::share('pagebeschrijving', 'pagebeschrijving'); ?>
@section('content')
<main>
	
    <article class="row">	
    	 <h2>Leaderboard</h2>  
   		<div class="col-lg-12">
   		
    	<p>Order by: </p> 
		
		<form id="orderform" name="orderform" action='leaderboard' method="post">
    	<select id="orderby" name="orderby">
    		<option value="username" <?php echo $order=='byname'?'selected':''; ?>>Name</option>
    		<option value="groepnaam" <?php echo $order=='bygroup'?'selected':''; ?>>Group</option>
    		<option value="amount" <?php echo $order=='byamount'?'selected':''; ?>>Amount</option>
    		<option value="avg_time" <?php echo $order=='byavgtime'?'selected':''; ?>>Avarage time</option>
    		<option value="stairs" <?php echo $order=='byttlstairs'?'selected':''; ?>>Total stairs</option>
    		<option value="cal" <?php echo $order=='bycal'?'selected':''; ?>>Calories</option>
    	</select>	

    	<select id="ordertype" name="ordertype">
    		<option value="asc" <?php echo $ordertype=='asc'?'selected':''; ?>>ASC</option>
    		<option value="desc" <?php echo $ordertype=='desc'?'selected':''; ?>>DESC</option>
    	</select>
    	</form>

    
		<div class="row">
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-1 head">
				Nr.
			</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
				Name
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
				Group
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-1 head">
				Aantal 
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
				Avarage time
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 visible-lg head">
				Total stairs
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 visible-lg head">
				Total cal burned
		</div>

		</div>
		<div id="scoredata">
		<?php $i=1; ?>
		@foreach($scoreboard as $move)
			
			<div class="row data">
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-1">
				<?php echo $i ?>
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				{{$move->username}}
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				{{$move->groepnaam}}
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-1">
				{{$move->amount}} 
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				{{$move->avg_time}}"
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 visible-lg">
				{{$move->stairs}}
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 visible-lg">
				{{$move->cal}}
			</div>
			</div>
			
			<?php $i++; ?>
		@endforeach
		</div>
	
		</div>
	
	</article>

<script type="text/javascript" src="{{URL::asset('js/ajaxCalls.js')}}"></script>
</main>
@stop
