@extends('layout')
<?php View::share('pagetitle', 'pagetitle'); ?>
<?php View::share('pagebeschrijving', 'pagebeschrijving'); ?>


@section('content')
<main>



    <article class="row">
	
    <h2>Inloggen</h2>  <div class="col-lg-12">
    
	<div id="formulier">
    <div id="validation-errors" class="alert alert-danger">
    

</div>  
	{{ Form::open( array(
    		'route' => 'login',
   		    'method' => 'post',
                'id' => 'loginform'
	))}}

	<p>

	@if (Auth::check())

		<p>Welkom, {{Auth::user()->voornaam}} {{Auth::user()->naam}}. U bent ingelogd.</p>

		<p><a id="uitloggen" href="logout">Uitloggen...</a></p>

	@endif

	</p>

	@if (!Auth::check())
	<div id="content">
	{{Form::text('emailadres', '', array(
		'id' => 'emailadres',
		'class'=>"form-control",
		'placeholder'=>'Emailadres:'
	))}}

	{{Form::password('wachtwoord', array(
		'id' => 'wachtwoord',
		'class'=>'form-control',
		'placeholder'=>'Wachtwoord:'
	))}}

	<p>Onthouden: {{ Form::checkbox('onthouden', '1', array(
		'id' => 'onthouden',
	)) }}</p>
	</div>
	{{ Form::submit( 'Inloggen', array(
    'id' => 'btn-login',
    'class' => 'btn btn-info btn-xs form-control'
	))}}
	{{Form::close()}}
	
	@endif
	
	
	@if(!Auth::check())
	<h3>Heb je nog geen account?</h3><p>
	<a style='width:190px;height:33px; line-height:29px;' class='btn btn-success btn-xs' href="{{URL::to('aanmelden')}}">Aanmelden</a></p>
	</div>
	@endif
</div>
	</article>



<script type="text/javascript" src="{{URL::asset('js/ajaxCalls.js')}}"></script>

</main>
@stop
