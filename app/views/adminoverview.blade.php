
<div class="row">
<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 head">
	Id.
</div>
<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 head">
	Naam
</div>
<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 head">
	wijzigen
</div>
<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 head">
	delete 
</div>
</div>

<div id="admindata">
		@foreach($data as $input)
			<div class="row data">
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
				{{$input->id}}
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
				@if(\Request::is('admin/leden*'))
					{{$input->naam}} {{$input->voornaam}}
				@elseif(\Request::is('admin/challenge*'))
					{{$input->naam}}
				@else
					{{$input->title}}
				@endif
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
				
				<a href="{{\Request::url()}}/{{$input->id}}" class="glyphicon glyphicon-pencil"></a>
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
				<a href="#" value="{{$input->id}}" class="glyphicon glyphicon-remove"></a>
			</div>
			</div>
		@endforeach
		{{$data->links()}}
</div>
