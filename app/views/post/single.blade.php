@extends('layout')
<?php View::share('pagetitle', 'pagetitle'); ?>
<?php View::share('pagebeschrijving', 'pagebeschrijving'); ?>
@section('content')

<main>
    <div id="slider">
        <div class="row">
            <div class="col-lg-12">
      <h2 class="uppercase">This is a big bold call to action!</h2>
      <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
</div>
<div id="sliderbuttons">
<a href="#" class="btn btn-warning btn-large">Get involved</a>
<a href="#" class="btn btn-danger btn-large"> MadeWithLove</a> 
</div>
</div>
</div>

<article class="row">
<h2>{{$post->title}}</h2>
<div class="cleared twopartsyntaxbox language-html white-box"><p><span>Author:</span> {{$user[0]->voornaam}} {{$user[0]->naam}} <span>Date:</span> {{date('d-m-Y', strtotime($post->created_at))}} </p></div>
<div class="media-body" ><img class="pull-left foto" alt="{{$post->title}} foto" src="{{url('/')}}{{$post->foto}}"/>
<p>{{$post->article}}</p>

</div>

</article>	
</main>
@stop