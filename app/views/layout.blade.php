@section('layout')
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  
  <title>Traphetaf.be | {{$pagetitle}}</title>
  <meta name="description" content="{{$pagebeschrijving}}">
  <meta name="viewport" content="width=device-width">
  <script type="text/javascript" src="{{URL::asset('/js/jquery1.8.min.js')}}"></script>

@yield('styles')   
<link media="all" type="text/css" rel="stylesheet" href="{{URL::asset('/styles/bootstrap.min.css');}}">
<link media="all" type="text/css" rel="stylesheet" href="{{URL::asset('/styles/style.css');}}">
</head>
<body>
 <div id="container"> 
  
    <nav class="navbar navbar-inverse" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <div id="logo" ></div>
              <h1><a class="navbar-brand" href="http://localhost" style="color:white">Trapetaf.be</a></h1>
            </div>

            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
                <li class="{{ Request::is( 'history') ? 'active' : '' }}"><a href="{{URL::to('history')}}">History</a></li>
                <li class="{{ Request::is( 'leaderboard') ? 'active' : '' }}"><a href="{{URL::to('leaderboard')}}">Leaderboard</a></li>
                <li class="{{Request::is('challenges/*')  ? 'active' : '' }}"><a href="{{URL::to('challenges')}}">Challenges</a></li>
                <li class="{{Request::is('statistics*')? 'active': ''}}"><a href="{{URL::to('statistics')}}">Statistics</a></li>
                <li class="{{ Request::is( 'login*') || Request::is('aanmelden*') ? 'active' : '' }}"><a href="{{URL::to('login')}}" >Inloggen</a></li>
	               @if(Auth::check())
                    <li class="{{ Request::is( 'admin*') ? 'active' : '' }}"><a href="{{URL::to('admin')}}">Admin</a></li>
                 @endif
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container -->
        </nav>

 @yield('content')

</div>

      <footer>
        <div class="row">
          <div class="col-md-12">
           <p> &copy; <?php echo Date('Y')?> Trapetaf.be</p>
          </div>
        </div>
      </footer> 
  @yield('scripts')
  @if(\Request::is('statistics'))
  <script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="http://www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
<script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
<script type="text/javascript" src="http://www.amcharts.com/lib/3/themes/none.js"></script>
<script>
AmCharts.ready(function () {
        makeLineChart();
        makePieChart();
        makeBarChart();
      });

        var dataBar = [
        <?php $i=0;$j=count($stairsstats);?>
        @foreach($groupstats as $group)


         
          @if($i != $j-1 )
            {
              "naam": "{{$group->naam}}",
              "color":  @if($i%2 == 0) '#34495e', @else '#1bbc9b', @endif
              "aantal": {{$group->aantal}}
            },
          @else
            {
              "naam": "{{$group->naam}}",
              "color": @if($i%2 == 0) '#34495e', @else '#1bbc9b', @endif
              "aantal": {{$group->aantal}}

            }
          @endif
        <?php $i++; ?>
        @endforeach
         ];
        

        var dataProvider = [
        <?php $i=0;$j=count($stairsstats);?>
        @foreach($stairsstats as $data)
         <?php $date = new \DateTime($data->date); ?>

          @if($i != $j-1)
            {
              "date": "{{$date->format('d-m-Y')}}",
              "treden": {{$data->value}}
            },
          @else
            {
              "date": "{{$date->format('d-m-Y')}}",
              "treden": {{$data->value}}

            }
          @endif
        <?php $i++; ?>
        @endforeach
         ];

      var previouslyHovered;

      function makeLineChart() {
        // SERIAL CHART
        var lineChart = new AmCharts.AmSerialChart();
        lineChart.type = "serial";
        lineChart.theme = "none";
        lineChart.dataProvider = dataProvider;
        lineChart.pathToImages = "http://www.amcharts.com/lib/3/images/";
        lineChart.dateDateFormat = "DD-MM-YYYY";
        lineChart.categoryField = "date";
        
        


        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.position = "left";
        valueAxis.axisAlpha = 0;
        lineChart.addValueAxis(valueAxis);

        // AXES

        // category
        var categoryAxis = lineChart.categoryAxis;
       
        categoryAxis.dashLength = 1;
       
        categoryAxis.minorGridEnabled = true;
      categoryAxis.position = 'top';

        // GRAPH
        var graph = new AmCharts.AmGraph();
        graph.valueField = "treden";
        graph.bulletBorderAlpha = 1;
        graph.bulletSize = 5;
        graph.hideBulletscount = 50;
        graph.lineColor = "#34495e";
        graph.lineThickness = 2;
        graph.bullet = "round";
        graph.bulletColor = "#1bbc9b";
        graph.useLineColorForBulletBorder = false;
        graph.id = 'g1';

        lineChart.addGraph(graph);

        //Scrollbar

        var scrollbar = new AmCharts.ChartScrollbar();
        scrollbar.graph = 'g1';
        scrollbar.scrollbarHeight=30;
        lineChart.addChartScrollbar(scrollbar);
        // CURSOR
        var chartCursor = new AmCharts.ChartCursor();
        
        chartCursor.cursorPosition = "mouse";
        chartCursor.pan = true;
        
        lineChart.addChartCursor(chartCursor);

        lineChart.write("chartdiv");
        $('#chartdiv > div > div').find('a').remove();
      }

      function zoomChart(){
    chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
}

var piedata = [{
        "title": "Treden omhoog",
        "value": {{$stairsupdownstats[0]}},
        'pulled':true
    }, {
        "title": "Treden omlaag",
        "value": {{$stairsupdownstats[1]}},
        'pulled':true
    }];

function makePieChart() {
        var chart = new AmCharts.AmPieChart();
        chart.theme = "none";
        chart.type = "serial";
        chart.colors = ["#34495e", "#1bbc9b"];
        chart.fontSize = 18;
        chart.fontFamily = 'Covered By Your Grace';
        
        chart.pulledField = "pulled";
        
        chart.dataProvider = piedata;
        chart.titleField = "title";
        chart.valueField = "value";
        
        chart.labelRadius = 5;
        chart.innerRadius = "60%";
        chart.radius = "42%";
        chart.balloonText = "[[title]]: [[percents]]%";

        // WRITE
        chart.write("piediv");
        $('#piediv > div > div').find('a').remove();

      }

function makeBarChart() {
        // SERIAL CHART
        var chart = new AmCharts.AmSerialChart();
        chart.dataProvider = dataBar;
        chart.rotate = true;
        chart.fontSize = 16;
        chart.fontFamily = 'Berkshire Swash';
        chart.categoryField = "naam";
        chart.startDuration = 0;
        chart.columnWidth = 0.5;

        

        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.minimum = 0;
        valueAxis.title = "Aantal keren trap genomen";
        chart.addValueAxis(valueAxis);

        // AXES
        // category
        var categoryAxis = chart.categoryAxis;
        categoryAxis.labelRotation = 90;
        categoryAxis.gridPosition = "start";
        categoryAxis.gridAlpha = 0;

        // GRAPH
        var graph = new AmCharts.AmGraph();
        graph.valueField = "aantal";
        graph.type = "column";
        graph.balloonText = "[[naam]]: [[aantal]]";
      
        graph.colorField = "color";
        graph.fillAlphas = 0.7;
        graph.lineThickness = 1;
        graph.lineColor = '#000000'
        chart.addGraph(graph);

        // CURSOR
        var chartCursor = new AmCharts.ChartCursor();
        chartCursor.cursorAlpha = 0;
        chartCursor.zoomable = false;
        
        chartCursor.categoryBalloonEnabled = false;
        chart.addChartCursor(chartCursor);

        chart.write("bardiv");
         $('#bardiv > div > div').find('a').remove();
      }


</script>

@endif
<script type="text/javascript" src='//code.jquery.com/jquery-1.10.2.min.js'></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
</body>  
</html>