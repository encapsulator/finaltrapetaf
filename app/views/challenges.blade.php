@extends('layout')
@section('content')<main>


<article class="row">
<h2>Challenges</h2>

<?php $tellera = 0; $tellerb = 0;?>
@foreach($activeChallenges as $challenge)

@if($tellera==0)
<h3>Active challenges</h3>
@endif
<div class="cleared twopartsyntaxbox language-html white-box"><p class="pull-right"><span>Type:</span> {{isset($challenge->timeInterval)?"wederkerend":"eenmalig"}} <span>Date:</span> {{date('d-m-Y', strtotime($challenge->datestart))}} </p><p><span>{{$challenge->naam}}</span></p> </div>
<div class="media-body" ><img alt="{{$challenge->naam}}" class="pull-left " src="{{url('/')}}{{$challenge->thumbnail}}"/>
<p>{{$challenge->beschrijving}}</p><a href="{{url('/')}}/challenges/{{str_replace(" ","_",$challenge->naam)}}" class="btn btn-info btn-xs">Lees meer...</a>
</div>
<?php $tellera++; ?>
@endforeach 

{{$activeChallenges->links()}}

@foreach($inactiveChallenges as $challenge)
@if($tellerb==0)
<h3>Inactive challenges</h3>
@endif

<div class="cleared twopartsyntaxbox language-html white-box"><p class="pull-right"><span>Type:</span> {{isset($challenge->timeInterval)?"wederkerend":"eenmalig"}} <span>Date:</span> {{date('d-m-Y', strtotime($challenge->datestart))}} </p><p><span>{{$challenge->naam}}</span></p> </div>
<div class="media-body" ><img alt="{{$challenge->naam}}" class="pull-left thumby " src="{{url('/')}}{{$challenge->thumbnail}}"/>
<p>{{$challenge->beschrijving}}</p><a href="{{url('/')}}/challenges/{{str_replace(" ","_",$challenge->naam)}}" class="btn btn-info btn-xs">Lees meer...</a>
</div>
<?php $tellerb++; ?>
@endforeach
{{$inactiveChallenges->links()}}
</article>	
</main>
@stop
