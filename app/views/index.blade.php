@extends('layout')
<?php View::share('pagetitle', 'pagetitle'); ?>
<?php View::share('pagebeschrijving', 'pagebeschrijving'); ?>
@section('content')

<main>
    <div id="slider">
        <div class="row">
            <div class="col-lg-12">
      <h2 class="uppercase">Verbeter je conditie en trap het af!</h2>
      <p>"Met dit project willen we werknemers motiveren vaker de trappen te nemen in de plaats van de lift. Dat heeft zo zijn
      	voordelen: een toegenomen conditie, meer spierkracht, beweeglijkere gewrichten en een energiekere geest. Elke dag de
      	trap nemen maakt wel degelijk het verschil. Ben je werkzaam in De Hoorn in Leuven? Doe met ons mee."</p>
</div>
<div id="sliderbuttons">
<a href="{{URL::to('aanmelden')}}" class="btn btn-warning btn-large">Get involved</a>
<a href="http://www.madewithlove.be" class="btn btn-danger btn-large"> MadeWithLove</a> 
</div>
</div>
</div>

<article class="row">
<h2>Laatste nieuws</h2>

@foreach($posts as $post)

<div class="cleared twopartsyntaxbox language-html white-box"><p class="pull-right"><span>Author:</span>@if(isset($post->naam)) {{$post->voornaam}} {{$post->naam}} @else Automatic Notification @endif   <span>Date:</span> {{date('d-m-Y', strtotime($post->created_at))}} </p><p><span>{{$post->title}}</span></p> </div>
<div class="media-body" >
@if($post->challenge==NULL || $post->challenge=='0') <img class="pull-left thumby" src="{{url('/')}}{{$post->thumbnail}}" alt="{{$post->title}} thumnail"/> @endif
<p >{{$post->beschrijving}}</p><a class="btn btn-info btn-xs" href="@if($post->challenge===NULL || $post->challenge=='0'){{url('/')}}/post/{{$post->id}} @else {{url('/')}}/challenges/{{$post->challenge}}@endif">
Lees meer...</a>
</div>
@endforeach    
{{$posts->links()}}
</article>	
</main>
@stop
