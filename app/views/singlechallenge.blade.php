@extends('layout')

@section('content')
<main>
	

    <article class="row">	
    	 <h2>{{$challenge->naam}}</h2>  
   		<div class="col-lg-12">
 		

 		<p><span>Startdate:</span>
 		@if(isset($challenge->timeInterval))
 			<?php $startdate = new Datetime($challenge->startDateLastInterval);
 				  echo $startdate->format("d-m-Y");
 			?>
 		@else
 			<?php $startdate = new Datetime($challenge->datestart);
 				  echo $startdate->format("d-m-Y");
 			?>
 		@endif
 		<span>Enddate:</span>
 		@if(isset($challenge->timeInterval))
 			<?php 
				  $dateInter = new DateInterval('P'. $challenge->timeInterval .'D');
				  $enddate = $startdate->add($dateInter);
				  echo $enddate->format('d-m-Y');
			?>
		@else
			<?php
				$datetime = new DateTime($challenge->dateend);
				echo $datetime->format("d-m-Y");
			?>
		@endif
		<br/>
		<span>Ranking:</span>
		@if ($challenge->ordering == "mostStairsStrategy")
			<?php echo $challenge->reverse==0?'most stairs taken.':'least stairs taken.'; ?>
		@endif
		@if ($challenge->ordering == "fastestTimePerStairStrategy")
			<?php echo $challenge->reverse==0?'fastest time per stair.':'slowest time per stair.'; ?>
		@endif
		@if ($challenge->ordering == "fastestTimePerStairUpStrategy")
			<?php echo $challenge->reverse==0?'fastest time per stair up.':'slowest time per stair up.'; ?>
		@endif	
		@if ($challenge->ordering == "fastestTimePerStairDownStrategy")
			<?php echo $challenge->reverse==0?'fastest time per stair down.':'slowest time per stair down.'; ?>
		@endif
		@if ($challenge->ordering == "mostStairsUpPerStairStrategy")
			<?php echo $challenge->reverse==0?'most amount of stairs up per stair.':'least amount of stairs up per stair.'; ?>
		@endif
		@if ($challenge->ordering == "mostTimesDownStrategy")
			<?php echo $challenge->reverse==0?'most times taken stairs down.':'least times taken stairs down.'; ?>
		@endif
		@if ($challenge->ordering == "mostTimesUpStrategy")
			<?php echo $challenge->reverse==0?'most times taken stairs up.':'least times taken stairs up.'; ?>
		@endif
		@if ($challenge->ordering == "mostStairsDownStrategy")
			<?php echo $challenge->reverse==0?'most total stairs down.':'least total stairs down.'; ?>
		@endif
		@if ($challenge->ordering == "mostStairsUpStrategy")
			<?php echo $challenge->reverse==0?'most total stairs up.':'least total stairs up.'; ?>
		@endif  
		@if($challenge->ordering == "mostCaloriesBurnedStrategy")
 			<?php echo $challenge->reverse==0?'most calories burned.':'least calories burned.'; ?>
		@endif
		@if($challenge->ordering=="fastestAvarageTimeStrategy")
 			<?php echo $challenge->reverse==0?'fastest avarage time.':'slowest avarage time.'; ?>
		@endif
		@if($challenge->ordering=="mostTimesTakenStairsStrategy")
			<?php echo $challenge->reverse==0?'most times taken stairs.':'least times taken stairs.'; ?>
		@endif
 		</p>

 		<img src="{{URL::to('/')}}{{$challenge->foto}}" class="foto" alt="$challenge->naam"><p> {{$challenge->beschrijving}}</p>
 		<div style="clear:both;"></div>
 		<h3>Huidige ranking</h3>

		<div class="row">
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-1 head">
				Nr.
			</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
				Name
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
				Group
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-1 head">
				Amount
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 visible-lg head">
				Avarage time
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 visible-lg head ">
				Total stairs
		</div>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 head">
			@if ($challenge->ordering == "mostStairsStrategy" || $challenge->ordering == "mostCaloriesBurnedStrategy" || $challenge->ordering=="fastestAvarageTimeStrategy" || $challenge->ordering=="mostTimesTakenStairsStrategy")
				Total cal burned
			@endif
			@if ($challenge->ordering == "fastestTimePerStairStrategy")
				Time per stair
			
			@endif
			
			@if ($challenge->ordering == "fastestTimePerStairUpStrategy")
				Time per stair up
			
			@endif
			
			@if ($challenge->ordering == "fastestTimePerStairDownStrategy")
				Time per stair down
			@endif
			
			@if ($challenge->ordering == "mostStairsUpPerStairStrategy")
				Stairs up per stair
			@endif
			
			@if ($challenge->ordering == "mostTimesDownStrategy")
				Times down
			@endif
			
			@if ($challenge->ordering == "mostTimesUpStrategy")
				Times up
			@endif
			
			@if ($challenge->ordering == "mostStairsDownStrategy")
				Amount down
			@endif
			@if ($challenge->ordering == "mostStairsUpStrategy")
				Amount up
			@endif
		</div>

		</div>
		<div id="scoredata">
		<?php $i=1; ?>
		@foreach($scoreboard as $entry)

			<div class="row data">
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-1">
				<?php echo $i ?>
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				{{$entry->username}}
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				{{$entry->groepnaam}}
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-1">
				{{$entry->amount}} 
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
				{{$entry->avg_time}}''
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 visible-lg">
				{{$entry->stairs}}
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 visible-lg">
				{{$entry->special}}
			</div>
			</div>
			<?php $i++; ?>
		@endforeach
		@if($i==1)
			<p>No data available</p>
		@endif
		</div>
		@if($previousIntervals!=="no intervals" && count($previousIntervals)>1)
		<?php $i=0; $j = count($previousIntervals); ?>
		<h3>Previous intervals</h3>
		<ul>
		@foreach($previousIntervals as $previousInterval)
			@if($i!=0)
			<li><a href="{{URL::current()}}/{{$j}}">{{$i}}. {{$previousInterval[0]->format("d-m-Y")}} {{$previousInterval[1]->format("d-m-Y")}}</a></li>
			@endif
			<?php $i++; $j--;?>
		@endforeach
		</ul>
		@endif
		</div>
	
	</article>

<script type="text/javascript" src="{{URL::asset('js/ajaxCalls.js')}}"></script>
</main>
@stop
