@extends('layout')
<?php View::share('pagetitle', 'pagetitle'); ?>
<?php View::share('pagebeschrijving', 'pagebeschrijving'); ?>

@section('content')
<main>
	

    <article class="row">
	
    <h2>Aanmelden</h2>  <div class="col-lg-12">  
	<div id="formulier">
		
@if(isset($success))

	<div id="validation-errors" class="alert @if($success) alert-success @else alert-danger @endif" style="display:block;">
<a class='close' href='#'>×</a>
	@if($success)
		<p>Je bent succesvol aangemeld, je kan nu inloggen!</p>
	@else
		<p>Facebookregistratie mislukt.</p>
	@endif

</div>  

@else

	<div id="validation-errors" class="alert alert-danger">

	</div>  
@endif
	<div id="content">
	<a href="#" id = "facebook" style="margin-bottom:15px;" class="btn btn-primary btn-sm">Aanmelden met Facebook</a>
	{{ Form::open( array(
    		'route' => 'aanmelden',
   			'method' => 'post',
            'id' => 'aanmeldform'
	))}}

	{{Form::text('voornaam', '', array(
		'id' => 'voornaam', 'class'=>'form-control',
		'placeholder'=>"Voornaam*:"
	))}}
	{{Form::text('naam','', array(
		'id' => 'naam',
		'class'=>'form-control',
		'placeholder'=>'Naam*:'
	))}}
	

	{{ Form::text('email', '', array(
		'id' => 'email',
		'class'=>'form-control',
		'placeholder'=>'Emailadres*:'
	)) }}
	

	{{ Form::text('password', '', array(
		'id' => 'password',
		'placeholder'=>'Wachtwoord*:',
		'class'=>'form-control'
	)) }}
	
	{{ Form::text('passwordCheck', '', array(
		'id' => 'passwordCheck',
		'placeholder'=>'Wachtwoord opnieuw*:',
		'class'=>'form-control'
	)) }}

	{{ Form::select('groep', $groep_opties , '',
		array('class'=>'form-control'
		)
	) }}
	
	{{ Form::text('adres', '', array(
		'id' => 'adres',
		'class'=>'form-control',
		'placeholder'=>'Adres:'
	)) }}
	
	{{ Form::text('woonplaats', '', array(
		'id' => 'woonplaats',
		'class'=>'form-control',
		'placeholder'=>'Woonplaats:'
	)) }}
	
	
	{{ Form::text('geboortedatum', '', array(
		'id' => 'geboortedatum',
		'class'=>'form-control',
		'placeholder'=>'Geboortedatum: (dd-mm-YYYY)'
	)) }}
	


{{ Form::text('telefoonnummer', '', array(
		'id' => 'telefoonnummer', 'class'=>"form-control",
		'placeholder'=>"Telefoonnummer"
	)) }}
	

	
	<p>Velden met een * zijn verplicht.</p>
	
	

	{{ Form::submit( 'Aanmelden', array(
    'id' => 'btn-meldaan',
    'class' => 'btn btn-info btn-xs form-control'
)) }}
	{{Form::close()}}
	
	</div>
	</div>

	 <?php
$API_KEY = '1437179829892419';

$redirect_URI = 'http://localhost/facebookregister';
?>
 
<iframe id="fbregister" src="http://www.facebook.com/plugins/registration?
         client_id=<?php echo $API_KEY; ?>&
         redirect_uri=<?php echo $redirect_URI; ?>&
         fb_only=false&
         target=_SELF&
         fields=[
    {'name':'name'},
    {'name':'birthday'},
    {'name':'location'},
    {'name':'email'},
    {'name':'groep', 'description':'Groep', 'type':'select', 'options':<?php echo '{'; 
$i = 0;
   foreach($groep_opties as $key => $value) {
  if($i == 0){

  echo "'$key':'$value'";
} else {
	echo ", '$key':'$value'";	
}
  $i++;
}

     echo '}'?>},

    {'name':'password'}
]"
    scrolling="true"
    frameborder="no"
    
    allowTransparency="true"
    width="100%"
    
    >
</iframe>

</div>
	</article>


<script type="text/javascript" src="{{URL::asset('js/ajaxCalls.js')}}"></script>

</main>
@stop
