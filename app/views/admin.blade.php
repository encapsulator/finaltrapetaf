@extends('layout')
<?php View::share('pagetitle', 'pagetitle'); ?>
<?php View::share('pagebeschrijving', 'pagebeschrijving'); ?>


@section('content')
<main>



    <article class="row">
	
    <h2>{{$title}}</h2>  
    <div class="col-lg-12">
    
    <ul class="nav nav-tabs" role="tablist" id="tabs">
       <li  {{ Request::is( 'admin') ? 'class="active"' : '' }}><a href="{{URL::to('admin')}}" class="home" role="tab" data-toggle="tab">Admin</a></li>
      <li  {{ Request::is( 'admin/profiel') ? 'class="active"' : '' }}><a href="{{URL::to('admin/profiel')}}" class="profiel" role="tab" data-toggle="tab">Profiel</a></li>
      @if(\Auth::user()->admin == 1)
      <li {{ Request::is( 'admin/leden*') ? 'class="active"' : '' }}><a href="{{URL::to('admin/leden')}}" class="leden" role="tab" data-toggle="tab">Leden</a></li>
      <li {{ Request::is( 'admin/batch') ? 'class="active"' : '' }} ><a href="{{URL::to('admin/batch')}}" class="batch" role="tab" data-toggle="tab">Batch toewijzen</a></li>
      <li class="dropdown {{ Request::is( 'admin/post*') ? 'active' : '' }}">
  	     <a href="#" id="postsdrop" class="dropdown-toggle" data-toggle="dropdown">Posts <span class="caret"></span></a>
  	     <ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1">
          <li {{ Request::is( 'admin/post/new') ? 'class="active"' : '' }}><a href="{{URL::to('admin/post/new')}}#postdrop" class = "newpost" tabindex="-1" role="tab" data-toggle="tab">Nieuw</a></li>
          <li {{ Request::is( 'admin/post') ? 'class="active"' : '' }}><a href="#dropdown2" tabindex="-1" role="tab" data-toggle="tab" class="posts" >Overzicht</a></li>
        </ul>
      </li>
      <li class="{{ Request::is( 'admin/challenge*') ? 'active' : '' }} dropdown">
  	   <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown">Challenges <span class="caret"></span></a>
  	   <ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1">
          <li {{ Request::is( 'admin/challenge/new') ? 'class="active"' : '' }}><a href="#dropdown1" tabindex="-1" role="tab" class="newchallenge" data-toggle="tab">Nieuw</a></li>
          <li  {{ Request::is( 'admin/challenge') ? 'class="active"' : '' }}><a href="#dropdown2" tabindex="-1" role="tab" class="challenges" data-toggle="tab">Overzicht</a></li>
      </ul>
      </li>
      @endif
    </ul>

    <div id="validation-errors" class="alert alert-danger">
    </div>

    <div id="admintabcontent">
   {{$form}}
  </div>


  </div>
</article>

</main>
<script>
  $(function(){
     



{{$ajax}}

        $('body').on('click', 'a.newpost', function() {
            window.location.href = "{{URL::to('admin')}}/post/new";
        });
        $('body').on('click', 'a.home', function() {
             window.location.href = "{{URL::to('admin')}}";  
        });
        $('body').on('click', 'a.batch', function(){
            window.location.href = "{{URL::to('admin/batch')}}";
        });
        $('body').on('click', 'a.profiel', function(){
            window.location.href = "{{URL::to('admin/profiel')}}";
        });
        $('body').on('click', 'a.leden', function(){
            window.location.href = "{{URL::to('admin/leden')}}";
        });
        $('body').on('click', 'a.posts', function(){
            window.location.href = "{{URL::to('admin/post')}}";
        });
        $('body').on('click', 'a.newchallenge', function(){
            window.location.href = "{{URL::to('admin/challenge/new')}}";
        });
        $('body').on('click', 'a.challenges', function(){
            window.location.href = "{{URL::to('admin/challenge')}}";
        });
  });
</script>

@stop
