@extends('layout')
<?php View::share('pagetitle', 'pagetitle'); ?>
<?php View::share('pagebeschrijving', 'pagebeschrijving'); ?>
<?php View::share('stairsstats', 'stairsstats'); ?>
<?php View::share('stairsupdownstats', 'stairsupdownstats'); ?>
<?php View::share('groupstats', 'groupstats'); ?>
@section('content')
<main>
    <article class="row">	
    	 <h2>Statistics</h2>  
   		<div class="col-lg-12">
		
		<h3>Trappen per dag</h3>

		<div id="chartdiv"></div>
		
		<h3>Verdeling trappen omhoog/omlaag</h3>
		<div id="piediv"></div>
		
		
		
		<h3>Fitste groepen voorbije week</h3>
		<div id="bardiv">

		</div>	
		</div>
	</article>

</main>

@stop