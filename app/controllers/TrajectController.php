<?php

use Repository\TrajectRepository;

class TrajectController extends Controller {
	
	private $trajectRepository;
	
	public function __construct( \Repository\TrajectRepository $trajectRepos ) {
		$this->trajectRepository = $trajectRepos;
	}
	/* Get all previous Trajects (movements) and render them in the history view */	
	public function getHistory( ) {
		$bewegingen = $this->trajectRepository->findAll();
		return View::make( 'history', array(
			'bewegingen' => $bewegingen,
			'pagetitle' => 'History',
			'pagebeschrijving' => "Bekijk een 
								   overzicht van alle laatste trajecten die zijn 
								   afgelegd op de trappen van De Hoorn." 
		) );
	}
	/* Get the all-time leaderboard based on all trajects */
	public function getLeaderboard( ) {
		$leaderboard = $this->trajectRepository->getLeaderboard();
		return View::make( "leaderboard", array( 
			'scoreboard' => $leaderboard,
			'order' => 'byamount',
			'ordertype' => 'DESC',
			'pagetitle' => 'Leaderbord',
			'pagebeschrijving' => 'Bekijk wie het meeste de trappen nam in De Hoorn op de rankingboard pagina van trapetaf.be.' 
		) );
	}
	/* Change leaderboardorder, order on given select input */
	public function changeLeaderboardOrder( ) {
		$leaderboard = $this->trajectRepository->changeOrder( Input::all() );
		$response    = array(
			"success" => true,
			"orderdata" => $leaderboard 
		);
		return Response::json( $response, 200 );
	}
}