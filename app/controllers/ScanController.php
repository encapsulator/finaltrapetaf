<?php

use Repository\ScanRepository;
use Repository\BootrecordRepository;

class ScanController extends Controller {
	
	private $scanRepository;
	private $bootRepository;
	
	public function __construct( \Repository\ScanRepository $scanRepository, \Repository\BootrecordRepository $bootRepository ) {
		$this->scanRepository = $scanRepository;
		$this->bootRepository = $bootRepository;
	}
	/* A card has been scanned, check conditions and put in database */	
	public function scan( $mifareid, $unitid ) {
		$result = false;
		
		$result = $this->scanRepository->make( array(
			'mifareid' => $mifareid,
			'unitid' => $unitid 
		) );
		
		//return 'Er ging iets mis. Contacteer de beheerder';
		if ( $result ) {
			return "Kaart succesvol gescand";
		} //$result
		return "Kaart niet gevonden in database";
	}
	/* Log unit in table bootrecord after boot */
	public function boot( $ipaddress, $unitid ) {
		$result = $this->bootRepository->make( array(
			 $unitid,
			$ipaddress 
		) );
		if ( $result ) {
			return "Device " . $unitid . " booted successfully";
		} //$result
		return "Could not save new bootrecord. Check Bootrecord model";
	}
}
