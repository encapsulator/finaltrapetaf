<?php
use FormBuilder\NewPostForm;
use FormBuilder\LinkUserCardForm;
use FormBuilder\UpdatePostForm;
use FormBuilder\HTMLForm;
use FormBuilder\UpdateProfileForm;
use FormBuilder\UpdateOwnProfileForm;
use FormBuilder\NewChallengeForm;
use FormBuilder\UpdateChallengeForm;
use Exceptions\ValidationException;

class AdminController extends Controller {
	
	private $repositories;
	
	public function __construct( \Repository\PostRepository $postRepos, \Repository\UserRepository $userRepos, \Repository\ChallengeRepository $challengeRepos ) {
		$this->repositories[ 'post' ]      = $postRepos;
		$this->repositories[ 'user' ]      = $userRepos;
		$this->repositories[ 'challenge' ] = $challengeRepos;
	}
	/* Build the form to add a new post */	
	public function buildPostForm( ) {
		$director = new NewPostForm( HTMLForm::createBuilder( "addnewpost", URL::to( 'admin' ) . "/post/new" ) );
		$form     = $director->build();
		return View::make( 'admin', array(
			 "form" => $form[ 'form' ],
			"ajax" => $form[ 'ajax' ],
			"title" => "Add a new post!",
			'pagetitle' => "Adminpagina",
			'beschrijving' => "Beheer je account op de Trapetaf applicatie in De Hoorn." 
		) );
	}
	/* Build the form to update an existing form */
	public function buildUpdatePostForm( $id ) {
		$director = new UpdatePostForm( HTMLForm::createBuilder( "updatepost", URL::to( 'admin' ) . "/post/" . $id ), $id );
		$form     = $director->build();
		return View::make( 'admin', array(
			 "form" => $form[ 'form' ],
			"ajax" => $form[ 'ajax' ],
			"title" => "Update a post!",
			'pagetitle' => "Adminpagina",
			'beschrijving' => "Beheer je account op de Trapetaf applicatie in De Hoorn." 
		) );
	}
	/* Build the form to link a registered user to an unassigned card */
	public function buildLinkUserCardForm( ) {
		$director = new LinkUserCardForm( HTMLForm::createBuilder( "linkusercard", URL::to( 'admin' ) . "/batch" ) );
		$form     = $director->build();
		return View::make( 'admin', array(
			 'form' => $form[ 'form' ],
			"ajax" => $form[ 'ajax' ],
			"title" => "Batch toewijzen",
			'pagetitle' => "Adminpagina",
			'beschrijving' => "Beheer je account op de Trapetaf applicatie in De Hoorn." 
		) );
	}
	/* Submit the form to add a new post */
	public function newPostSubmit( ) {
		try {
			$result = $this->repositories[ 'post' ]->make( Input::all() );
		}
		catch ( ValidationException $e ) {
			$errors = explode( "&", $e->getMessage() );
			return Response::json( array(
				 'success' => false,
				'errors' => $errors 
			) );
		}
		if ( $result ) {
			return Response::json( array(
				 'success' => true 
			), 200 );
		} //$result
		$response = array(
			 'success' => false,
			'errors' => array(
				 'newpost' => array(
					 "Oeps.. Er ging iets mis, probeer opnieuw." 
				) 
			) 
		);
		return \Response::json( $response );
	}
	/* Submit the form to update an existing post */
	public function updatePostSubmit( $id ) {
		try {
			$result = $this->repositories[ 'post' ]->update( $id, Input::all() );
		}
		catch ( ValidationException $e ) {
			$errors = explode( "&", $e->getMessage() );
			return Response::json( array(
				 'success' => false,
				'errors' => $errors 
			) );
		}
		if ( $result ) {
			return Response::json( array(
				 'success' => true 
			), 200 );
		} //$result
		$response = array(
			 'success' => false,
			'errors' => array(
				 'newpost' => array(
					 "Oeps.. Er ging iets mis, probeer opnieuw." 
				) 
			) 
		);
		return \Response::json( $response );
	}
	/* Just a welcoming message for the admin panel */
	public function main( ) {
		return View::make( 'admin', array(
			 "form" => "<p>Maak een keuze uit het bovenstaand menu</p>",
			"ajax" => "",
			"title" => "Admin",
			'pagetitle' => "Adminpagina",
			'beschrijving' => "Beheer je account op de Trapetaf applicatie in De Hoorn." 
		) );
	}
	/* Submit the form to link a registred user to an unassigned card */
	public function linkUserCard( ) {
		try {
			$this->repositories[ 'user' ]->linkUserCard( Input::get( 'user' ), Input::get( 'kaart' ) );
		}
		catch ( Exception $e ) {
			$response = array(
				 'success' => false,
				'errors' => array(
					 'linkUserCard' => ( "Oeps.. Er ging iets mis, probeer opnieuw." ) 
				) 
			);
			return \Response::json( $response );
		}
		
		return \Response::json( array(
			 'success' => true 
		), 200 );
		
	}
	/* Let's every registred user change their registering/login-details. When they are admin they have further access and
	can change the user privileges */
	public function buildProfileForm( ) {
		if ( Auth::user()->admin == 1 ) {
			return $this->updateMemberForm( Auth::user()->id );
		} //Auth::user()->admin == 1
		return $this->updateOwnAccountForm( Auth::user()->id );
	}
	/* Form to update an other users' profile */
	public function updateMemberForm( $id ) {
		$director = new UpdateProfileForm( HTMLForm::createBuilder( "updateprofile", URL::to( 'admin' ) . "/leden/" . $id ), $id );
		$form     = $director->build();
		return View::make( 'admin', array(
			 'form' => $form[ 'form' ],
			"ajax" => $form[ 'ajax' ],
			"title" => "Profiel wijzigen",
			'pagetitle' => "Adminpagina",
			'beschrijving' => "Beheer je account op de Trapetaf applicatie in De Hoorn." 
		) );
	}
	/* Submit the profile form, and update the profile of the logged in user */
	public function updateOwnAccountForm( $id ) {
		$director = new UpdateOwnProfileForm( HTMLForm::createBuilder( "updateprofile", URL::to( 'admin' ) . "/profiel/" . $id ), $id );
		$form     = $director->build();
		return View::make( 'admin', array(
			 'form' => $form[ 'form' ],
			"ajax" => $form[ 'ajax' ],
			"title" => "Profiel wijzigen",
			'pagetitle' => "Adminpagina",
			'beschrijving' => "Beheer je account op de Trapetaf applicatie in De Hoorn." 
		) );
	}
	/* Build the form to add a new challenge */
	public function buildNewChallengeForm( ) {
		$director = new NewChallengeForm( HTMLForm::createBuilder( "newchallenge", URL::to( 'admin' ) . "/challenge/new" ) );
		$form     = $director->build();
		return View::make( 'admin', array(
			 'form' => $form[ 'form' ],
			'ajax' => $form[ 'ajax' ],
			"title" => "Nieuwe challenge",
			'pagetitle' => "Adminpagina",
			'beschrijving' => "Beheer je account op de Trapetaf applicatie in De Hoorn." 
		) );
		
	}
	/* Build the form to update an existing challenge */
	public function buildUpdateChallengeForm( $id ) {
		$director = new UpdateChallengeForm( HTMLForm::createBuilder( "updatechallenge", URL::to( 'admin/challenge' ) . "/" . $id ), $id );
		$form     = $director->build();
		return View::make( 'admin', array(
			 'form' => $form[ 'form' ],
			'ajax' => $form[ 'ajax' ],
			'title' => "Challenge wijzigen",
			'pagetitle' => "Adminpagina",
			'beschrijving' => "Beheer je account op de Trapetaf applicatie in De Hoorn." 
		) );
	}
	/* Submit form to update your profile */
	public function updateOwnProfile( ) {
		return $this->updateProfile( Auth::user()->id );
	}
	/* Submit the form to update a certain users' profile */
	public function updateProfile( $id ) {
		$result = false;
		try {
			$result = $this->repositories[ 'user' ]->update( Input::all(), $id );
		}
		catch ( ValidationException $e ) {
			$errors = explode( "&", $e->getMessage() );
			return Response::json( array(
				 'success' => false,
				'errors' => $errors 
			) );
		}
		if ( $result ) {
			return Response::json( array(
				 'success' => true 
			), 200 );
		} //$result
		$response = array(
			 'success' => false,
			'errors' => array(
				 'newpost' => array(
					 "Oeps.. Er ging iets mis, probeer opnieuw." 
				) 
			) 
		);
		return \Response::json( $response );
		
	}
	/* Get an overview of all the members registered */
	public function membersOverview( ) {
		$members      = $this->repositories[ 'user' ]->userOverview();
		$memberColumn = View::make( 'adminoverview', array(
			 'data' => $members[ 0 ] 
		) );
		return View::make( 'admin', array(
			 'form' => $memberColumn,
			"ajax" => $members[ 1 ],
			"title" => "Leden",
			'pagetitle' => "Adminpagina",
			'beschrijving' => "Beheer je account op de Trapetaf applicatie in De Hoorn." 
		) );
	}
	/* Get an overview of all the posts */
	public function postsOverview( ) {
		$posts      = $this->repositories[ 'post' ]->postOverview();
		$postColumn = View::make( 'adminoverview', array(
			 'data' => $posts[ 0 ] 
		) );
		return View::make( 'admin', array(
			 'form' => $postColumn,
			"ajax" => $posts[ 1 ],
			"title" => "Posts" 
		) );
	}
	/* Delete a certain post */
	public function deletePost( ) {
		$result = $this->repositories[ 'post' ]->delete( \Input::get( 'id' ) );
		if ( $result ) {
			return \Response::json( array(
				 "success" => true 
			), 200 );
		} //$result
		$response = array(
			 'success' => false,
			'errors' => array(
				 'newpost' => array(
					 "Oeps.. Er ging iets mis, contacteer de webmaster." 
				) 
			) 
		);
		return \Response::json( $response );
	}
	/* Delete a certain user */
	public function deleteLid( ) {
		$result = $this->repositories[ 'user' ]->delete( \Input::get( 'id' ) );
		if ( $result ) {
			return \Response::json( array(
				 "success" => true 
			), 200 );
		} //$result
		$response = array(
			 'success' => false,
			'errors' => array(
				 'newpost' => array(
					 "Oeps.. Er ging iets mis, contacteer de webmaster." 
				) 
			) 
		);
		return \Response::json( $response );
	}
	/* Delete a certain challenge */
	public function deleteChallenge( ) {
		$result = $this->repositories[ 'challenge' ]->delete( \Input::get( 'id' ) );
		if ( $result ) {
			return \Response::json( array(
				 "success" => true 
			), 200 );
		} //$result
		$response = array(
			 'success' => false,
			'errors' => array(
				 'newpost' => array(
					 "Oeps.. Er ging iets mis, contacteer de webmaster." 
				) 
			) 
		);
		return \Response::json( $response );
	}
	/* List all challenges */
	public function challengeOverview( ) {
		$challenges      = $this->repositories[ 'challenge' ]->adminChallengeOverview();
		$challengeColumn = View::make( 'adminoverview', array(
			 'data' => $challenges[ 0 ] 
		) );
		return View::make( 'admin', array(
			'form' => $challengeColumn,
			"ajax" => $challenges[ 1 ],
			"title" => "Challenges",
			'pagetitle' => "Adminpagina",
			'beschrijving' => "Beheer je account op de Trapetaf applicatie in De Hoorn." 
		) );
	}
	/* Submit the form to add a new challenge */
	public function postNewChallenge( ) {
		
		$result = false;
		try {
			$result = $this->repositories[ 'challenge' ]->make( Input::all() );
		}
		catch ( ValidationException $e ) {
			$errors = explode( "&", $e->getMessage() );
			return Response::json( array(
				'success' => false,
				'errors' => $errors 
			) );
		}
		if ( $result ) {
			return Response::json( array(
				'success' => true 
			), 200 );
		} //$result
		$response = array(
			'success' => false,
			'errors' => array(
				 'newpost' => ( "Oeps.. Er ging iets mis, probeer opnieuw." ) 
			) 
		);
		return \Response::json( $response );
	}
	/* Submit the form to update an existing challenge */
	public function postUpdateChallenge( $id ) {
		$result = false;
		try {
			$result = $this->repositories[ 'challenge' ]->update( $id, Input::all() );
		}
		catch ( ValidationException $e ) {
			$errors = explode( "&", $e->getMessage() );
			return Response::json( array(
				'success' => false,
				'errors' => $errors 
			) );
		}
		if ( $result ) {
			return Response::json( array(
				'success' => true 
			), 200 );
		} //$result
		$response = array(
			'success' => false,
			'errors' => array(
				 'newpost' => array(
					 "Oeps.. Er ging iets mis, probeer opnieuw." 
				) 
			) 
		);
		return \Response::json( $response );
	}
}