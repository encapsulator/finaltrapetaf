<?php
use Repository\PostRepository;

class PostController extends Controller {
	private $postRepository;
	
	public function __construct( \Repository\PostRepository $postrepos ) {
		$this->postRepository = $postrepos;
	}
	/*Get an overview of the latest posts*/	
	public function getOverview( ) {
		$posts = $this->postRepository->findAll();
		return View::make( 'index', array(
			'posts' => $posts,
			'pagetitle' => "Home, laatste nieuws.",
			'pagebeschrijving' => "Trapetaf.be is een nieuwe en toffe applicatie in De Hoorn in Leuven, dat werknemers middels een RFID systeem aanmoedigt meer de trap te nemen!" 
		) );
	}
	/*Request a single post in detail*/
	public function getSingle( $id ) {
		$single = $this->postRepository->getPost( $id );
		return View::make( 'post/single', array(
			'post' => $single[ 'post' ],
			'user' => $single[ 'author' ],
			'pagetitle' => "Post: " . $single[ 'post' ]->title,
			'pagebeschrijving' => $single[ 'post' ]->beschrijving 
		) );
	}
}