<?php

class StatisticsController extends Controller {
	
	private $statisticsRepository;
	
	public function __construct( \Repository\StatisticsRepository $statrepos ) {
		$this->statisticsRepository = $statrepos;
	}
	/* Load the wanted statistics and render them in the statistics view */	
	public function getStatistics( ) {
		$totaltrappendata = $this->statisticsRepository->getStairsPerDayStats();
		$omhoogomlaagdata = $this->statisticsRepository->getTotalStairsUpDown();
		$fittest          = $this->statisticsRepository->getFittestGroups();
		return View::make( 'statistics', array(
			'stairsstats' => $totaltrappendata,
			'stairsupdownstats' => array(
				$omhoogomlaagdata[ 0 ][ 0 ]->omhoog,
				$omhoogomlaagdata[ 1 ][ 0 ]->omlaag 
			),
			'groupstats' => $fittest,
			'pagetitle' => 'Statistieken',
			'pagebeschrijving' => 'Bekijk de all-time statistieken van de Trapetaf applicatie in De Hoorn. Hoeveel mensen doen mee, nemen de trap en verbeteren hun gezondheid elke dag.' 
		) );
	}
	
}
