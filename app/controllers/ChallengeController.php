<?php

use Exceptions\ChallengeNotFoundException;
use Exceptions\ChallengeIntervalNotFoundException;

class ChallengeController extends Controller {
	
	private $challengeRepository;
	
	public function __construct( \Repository\ChallengeRepository $chalrepos ){
		$this->challengeRepository = $chalrepos;
	}

	/* Get challenge based on the name */
	public function getSingleChallenge( $naam ){

		$single;
		try {
			$single = $this->challengeRepository->getSingleChallenge( $naam );
		} catch (ChallengeNotFoundException $e){
			return View::make('404', array("title"=>"challenge not found", "message"=>"De challenge werd niet gevonden of bestaat niet meer. Keer terug naar de vorige pagina."));
		}
		
		return View::make( 'singlechallenge', array(
			"scoreboard"        => $single['scoreboard'],
			"challenge"         => $single['challenge'],
			"previousIntervals" => $single['previousIntervals'],
			'pagetitle'			=> 'Challenge: '. $single['challenge']->naam,
			'pagebeschrijving'  => $single['challenge']->beschrijving
		) );
	}

	/* Cronjob to check for end or new Interval of each challenge*/
	public function updateChallenges(){
		return $this->challengeRepository->updateChallenges();
	}

	/*Get the leaderboard of a particular interval of a challenge*/
	public function getIntervalSingleChallenge( $name, $interval ){

		$singleInterval;

		try {
			$singleInterval = $this->challengeRepository->getIntervalSingleChallenge( $name, $interval );
		} catch (ChallengeNotFoundException $e){
			return View::make('404', array("title"=>"challenge not found", "message"=>"De challenge werd niet gevonden of bestaat niet meer. Keer terug naar de vorige pagina."));	
		} 

		return View::make( 'singlechallengeinterval', array( 
			"scoreboard"       => $singleInterval["scoreboard"],
			"challenge"        => $singleInterval['challenge'],
			"interval"         => $singleInterval['interval'],
			"dateStart"		   => $singleInterval['dateStart'],
			"dateEnd"          => $singleInterval["dateEnd"],
			'pagetitle'		   => $singleInterval['challenge']->naam . ": intervalpagina.",
			'pagebeschrijving' => "Bekijk de resultaten van een specifiek interval van de Trapetaf " . $singleInterval['challenge']->naam . " challenge in De Hoorn."
		));

	}

	/*List all active and inactive challenges*/
	public function getOverview(){
		$challenges = $this->challengeRepository->getOverview();

		return View::make('challenges', array(
			"activeChallenges"   => $challenges["activeChallenges"],
			"inactiveChallenges" => $challenges["inactiveChallenges"],
			"pagetitle"			 => "Challenges in De Hoorn",
			'pagebeschrijving'	 => "Bekijk alle actieve en inactieve challenges, de resultaten, de winnaars en de verliezers, van de Trapetaf applicatie in De Hoorn"
		));
	}
	
}