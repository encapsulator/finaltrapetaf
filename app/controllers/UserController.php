<?php

use Repository\UserRepository;
use Exceptions\ValidationException;
class UserController extends Controller {
    
    private $userRepository;
    
    public function __construct( \Repository\UserRepository $userRepository ) {
        $this->userRepository = $userRepository;
    }
    /* Go to loginpage */    
    public function getLoginPage( ) {
        return View::make( 'login', array(
            'pagetitle' => 'Inloggen',
            'pagebeschrijving' => 'Inlogpagina trapetaf.be.' 
        ) );
    }
    /* Go to register page */
    public function getRegisterPage( ) {
        $groups = $this->userRepository->getAllGroups();
        return View::make( 'aanmelden', array(
            'groep_opties' => $groups,
            'pagetitle' => 'Aanmelden',
            'pagebeschrijving' => 'Meld je aan om mee te doen aan een gezondere werkdag en leuke challenges in De Hoorn.' 
        ) );
    }
    /* Handle registerform, try adding a user */
    public function addUser( ) {
        $result;
        try {
            $result = $this->userRepository->make( \Input::all() );
        }
        catch ( ValidationException $e ) {
            $errors = explode( "&", $e->getMessage() );
            return Response::json( array(
                 'success' => false,
                'errors' => $errors 
            ), 200 );
        }
        if ( $result ) {
            return Response::json( array(
                 'success' => true 
            ), 200 );
        } //$result
        $response = array(
            'success' => false,
            'errors' => array(
                 'newuser' => array(
                     "Oeps.. Er ging iets mis, probeer opnieuw." 
                ) 
            ) 
        );
        return \Response::json( $response );
    }
    /* Log out the user */
    public function logout( ) {
        $this->userRepository->logout();
        return Redirect::back();
    }
    /* Login post submitted, try loging user in */
    public function login( ) {
        $result = false;
        try {
            $result = $this->userRepository->login( Input::all() );
        }
        catch ( ValidationException $e ) {
            $errors = explode( "&", $e->getMessage() );
            return Response::json( array(
                'success' => false,
                'errors' => $errors 
            ), 200 );
        }
        
        if ( $result ) {
            return Response::json( array(
                'success' => true 
            ), 200 );
        } //$result
        
        $response = array(
            'success' => false,
            'errors' => array(
                 'login' => array(
                     "Onjuiste logingegevens." 
                ) 
            ) 
        );
        return \Response::json( $response );
    }
    /* Facebookform submitted, try adding the user */
    public function facebookRegister( ) {
        $result      = $this->userRepository->facebookRegister();
        $groepopties = $this->userRepository->getAllGroups();
        if ( $result ) {
            return View::make( 'aanmelden', array(
                'success' => true,
                'groep_opties' => $groepopties,
                'pagetitle' => 'Aanmelden',
                'pagebeschrijving' => 'Meld je aan om mee te doen aan een gezondere werkdag en leuke challenges in De Hoorn.' 
            ) );
        } //$result
        return View::make( 'aanmelden', array(
            'success' => false,
            'groep_opties' => $groepopties,
            'pagetitle' => 'Aanmelden',
            'pagebeschrijving' => 'Meld je aan om mee te doen aan een gezondere werkdag en leuke challenges in De Hoorn.' 
        ) );
    }
}