<?php

class Post extends BaseModel {
	
	protected $table = 'posts';
	protected $fillable = array( 'beschrijving', 'title', 'thumbnail', 'foto', 'article' );
	public function make( $data ) {
		$this->beschrijving = $data['beschrijving'];
		$this->title        = $data['title'];
		$this->thumbnail    = $data['thumbnail'];
		$this->foto         = $data['foto'];
		$this->article      = $data['article'];
		
		if(null!=$data['foto'] && null == \Input::file('thumbnail')){
			$this->thumbnail = $data['foto'];
		}

		if ( isset( $data['challenge'] ) ) {
			$this->challenge = $data["challenge"];
		}
		if ( isset( $data['id'] ) ) {
			$this->id = $data['id'];
		}
		if ( isset( $data['userid'] ) ) {
			$this->userid = $data['userid'];
		}
	}
	
	public function modify( $data ) {
		$this->make( $data );
	}
	
}