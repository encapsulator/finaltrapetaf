<?php
class Scan extends BaseModel
{
	public $timestamps = false;
	protected $table = 'scan';
	public function make( $input )
	{
		$this->mifareid = $input[ 'mifareid' ];
		$this->unitid   = $input[ 'unitid' ];
		$this->datetime = new Datetime( 'now' );
	}
	public function modify( $input )
	{
		$this->make( $input );
	}
}
