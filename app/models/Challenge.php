<?php
class Challenge extends BaseModel
{
	protected $table = 'challenge';
	public $timestamps = false;
	protected $fillable = array( 'startDateLastInterval', 'naam' );
	/*reverse = boolean die aangeeft of we de ChallengeStrategy
	moeten reversen (bvb. pijlen naar traagste, minste) =>
	in dat geval halen we rankingboard op via getReverseRankingBoard  
	*/
	/*the Interface ChallengeStrategy allows to change the way of ordering 
	data at runtime*/
	private $challengeStrategy;
	/*If not null the timeinterval specifies the interval on which all ranking will be reset.
	Ex: challenge which displays the most stairs taken every week. No 
	new challenge has to be created*/
	public function make( $data )
	{
		$this->modify( $data );
		if ( null !== $this->beschrijvingNew ) {
			$this->makeNewIntervalPost();
		}
	}
	public function modify( $data )
	{
		if ( array_key_exists( 'reverse', $data ) ) {
			$this->reverse = $data[ "reverse" ];
		} else {
			$this->reverse = 0;
		}
		$this->ordering = $data[ 'ordering' ];
		if ( null !== $data[ 'interval' ] ) {
			$this->timeInterval = $data[ 'interval' ];
		}
		$this->groepid = $data[ 'groepid' ];
		if ( null !== $data[ 'thumbnailfile' ] ) {
			$this->thumbnail = $data[ 'thumbnail' ];
		}
		if ( null !== $data[ 'fotofile' ] ) {
			$this->foto = $data[ 'foto' ];
		}
		if ( null != $data[ 'foto' ] && null == $data[ 'thumbnail' ] ) {
			$this->thumbnail = $data[ 'foto' ];
		}
		$this->beschrijving            = $data[ "beschrijving" ];
		$this->beschrijvingNewInterval = $data[ "beschrijvingNewInterval" ];
		$this->beschrijvingNew         = $data[ "beschrijvingNew" ];
		$this->beschrijvingEnd         = $data[ 'beschrijvingEnd' ];
		$this->datestart               = new \DateTime( $data[ 'datestart' ] );
		$this->dateend                 = new \DateTime( $data[ 'dateend' ] );
		$this->startDateLastInterval   = new \Datetime( $data[ 'datestart' ] );
		$this->naam                    = $data[ 'naam' ];
	}
	private function makeNewIntervalPost( )
	{
		$data = array(
			 "title" => "De " . $this->naam . " challenge werd gereset!",
			'beschrijving' => $this->beschrijvingNewInterval,
			"thumbnail" => $this->thumbnail,
			"challenge" => str_replace( " ", "_", $this->naam ),
			"foto" => null,
			"article" => null 
		);
		$this->createNewNotification( $data );
	}
	private function createNewNotification( $data )
	{
		$p = new \Post;
		$p->make( $data );
		$p->save();
	}
}