<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;


class User extends BaseModel implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $table = 'user';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $fillable = array('emailadres', 'wachtwoord');

	public $timestamps = false;

	public function getAuthPassword()
{
    return $this->wachtwoord;
}

public function getAuthIdentifier()
	{
		return $this->getKey();
	}

public function isAdmin()
	{
		return $this->admin == 1;
	}

public function getReminderEmail()
	{
		return $this->email;
	}
	public function setRememberToken($value)
	{
		$this->rememtok = $value;
	}

	public function getRememberToken()
	{
  		return $this->rememtok;  
	}
	public function getRememberTokenName()
	{
  		return 'rememtok';
	}

public function make($input){	
		$this->voornaam = $input['voornaam'];
		$this->naam = $input['naam'];
		$this->emailadres = $input['email'];
		$this->groepid = $input['groep'];
		$this->woonplaats = $input['woonplaats'];
		$this->geboortedatum = DateTime::createFromFormat('d-m-Y', $input['geboortedatum']);
		$this->wachtwoord = Hash::make($input['password']);
		$this->adres = $input["adres"];
		$this->admin = 0;
		$this->telefoonnummer = $input['telefoonnummer'];
	
}



public function modify($input){
	$this->voornaam = $input['voornaam'];
		$this->naam = $input['naam'];
		$this->emailadres = $input['emailadres'];
		$this->groepid = $input['groep'];
		$this->woonplaats = $input['woonplaats'];
		$this->geboortedatum = DateTime::createFromFormat('d-m-Y', $input['geboortedatum']);
		if( $input['password'] != null && $input['password'] != '' ){
			$this->wachtwoord = Hash::make($input['password']);
		}
		$this->adres = $input["adres"];
		$this->admin = $input['admin'];
		$this->telefoonnummer = $input['telefoonnummer'];
}

}