<?php
class Traject extends Eloquent
{
	protected $table = 'traject';
	public $timestamps = false;
	public function make( $data )
	{
		$this->userid = $data[ "userid" ];
		$datum        = new DateTime( $data[ 'datum' ] );
		$datum->format( "d-m-Y" );
		$this->datum         = $datum;
		$this->tijd          = $data[ 'tijd' ];
		$this->vanunitid     = $data[ 'vanunit' ];
		$this->naarunitid    = $data[ 'naarunit' ];
		$this->treden        = $data[ 'treden' ];
		$this->treden_omhoog = $data[ 'treden_omhoog' ];
	}
}