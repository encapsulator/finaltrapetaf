<?php
class Bootrecord extends BaseModel
{
	public $timestamps = false;
	protected $table = 'bootrecord';
	public function make( $input )
	{
		$this->unitid    = $input[ 'unitid' ];
		$this->ipaddress = $input[ 'ipaddress' ];
		$this->datetime  = new \Datetime( "now" );
	}
	public function modify( $input )
	{
		$this->make( $input );
	}
}