<?php

namespace Challenge;
use Challenge\ChallengeStrategy;
use App\models;

class MostStairsStrategy implements ChallengeStrategy {
	
	private $group;

	private $query;

	private $boolInterval;

	public setQuery($query){
		$this->query = $query;
	}

	public setQuery(){
		$this->query = "SELECT CONCAT(user.voornaam, ' ', user.naam) AS username, groep.naam AS groepnaam, count(traject.userid) as amount, 
					SUM(treden) AS stairs FROM user inner join traject on user.id = traject.userid inner join groep on groep.userid = user.id";
	}

	public setGroup($grp){
		$this->group = $grp;
	}

	public getTopN($amount){

		$query = $this->query + "WHERE traject.datestart >= ? AND traject.dateend <= ?";

		if($this->group!=0{
			$quer += " AND user.groepid = ? GROUP BY username HAVING COUNT(traject.userid)>0 ORDER BY SUM(traject.treden) DESC LIMIT ".$amount;
			$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s'), $this->group));
		} else {
			$quer += " GROUP BY username HAVING COUNT(traject.userid)>0 ORDER BY SUM(traject.treden) DESC LIMIT ".$amount;
			$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s')));
		}
		return $res;
	}

	public getWorstN($amount){

		$query = $this->query + "WHERE traject.datestart >= ? AND traject.dateend <= ?";

		if($this->group!=0{
			$quer += " AND user.groepid = ? GROUP BY username HAVING COUNT(traject.userid)>0 ORDER BY SUM(traject.treden) ASC LIMIT ".$amount;
			$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s'), $this->group));
		} else {
			$quer += " GROUP BY username HAVING COUNT(traject.userid)>0 ORDER BY SUM(traject.treden) ASC LIMIT ".$amount;
			$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s')));
		}
		return $res;
	
	}
	public getReverseRankingBetween($datetimefrom, $datetimeto){
		$query = $this->query + "WHERE traject.datestart >= ? AND traject.dateend <= ?";

		if($this->group!=0{
			$quer += " AND user.groepid = ? GROUP BY username HAVING COUNT(traject.userid)>0 ORDER BY SUM(traject.treden) ASC";
			$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s'), $this->group));
		} else {
			$quer += " GROUP BY username HAVING COUNT(traject.userid)>0 ORDER BY SUM(traject.treden) ASC";
			$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s')));
		}
		return $res;
	}
	
	public getRankingBetween($datetimefrom, $datetimeto){
		$query = $this->query + "WHERE traject.datestart >= ? AND traject.dateend <= ?";

		if($this->group!=0{
			$quer += " AND user.groepid = ? GROUP BY username HAVING COUNT(traject.userid)>0 ORDER BY SUM(traject.treden) DESC";
			$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s'), $this->group));
		} else {
			$quer += " GROUP BY username HAVING COUNT(traject.userid)>0 ORDER BY SUM(traject.treden) DESC";
			$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s')));
		}
		return $res;
	}
	
	public getCompetingUsersBetween($dtf, $dtt){
		$quer = "SELECT * FROM user inner join traject on user.id = traject.userid inner join groep on groep.id = user.groepid
			WHERE traject.datestart >= ? AND traject.dateend <= ?");
		
		if($this->group != 0){
			$quer += " AND user.groepid = ?";
			$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s'), $this->group));
		} else {
			$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s')));
		}	
		return $res;
	}
	public getAllWinners($timeInterval, $dtf){
		$now = new Datetime("now");
		$result = array();
		if($now->format("Y-m-d") > ($dtf->add($timeInterval))->format("Y-m-d")){
			while($now->format("Y-m-d") > ($dtf->add($timeInterval))->format("Y-m-d")){
				$datetimeto = $dtf->add($timeInterval);

				$quer = "SELECT * FROM user inner join traject on user.id = traject.userid inner join groep on groep.id = user.groepid
				WHERE traject.datestart >= ? AND traject.dateend <= ?");
		
				$query = $this->query + "WHERE traject.datestart >= ? AND traject.dateend <= ?";

				if($this->group!=0{
					$quer += " AND user.groepid = ? GROUP BY user.id ORDER BY SUM(traject.treden) DESC LIMIT 1";
					$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s'), $this->group));
				} else {
					$quer += " GROUP BY user.id ORDER BY SUM(traject.treden) DESC LIMIT 1";
					$res = DB::SELECT($quer, array(date_format($dtf, 'Y-m-d H:i:s'), date_format($dtt, 'Y-m-d H:i:s')));
				}

				$result = array_push($result, $res);
				$dtf = $dtf->add($timeInterval);
			}
			$this->createNewNotification($this->IntervalEndPost);
		}
	}

}